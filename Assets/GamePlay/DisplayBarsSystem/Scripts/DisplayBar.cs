﻿using UnityEngine;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine.UI;

public class DisplayBar : MonoBehaviour {

    [System.Serializable]
    public struct ColorBarStep {
        public string name;    
        [Range(0,1)] public float toRange;
        public Color color;
    }

    public enum BarEffectType { 
        Blink,
        ColorTransition
    }

    [SerializeField] Slider bar;
    [SerializeField] Text displayText;
    [Tooltip("From 0 to 100, Color Steps to asign to this display bar")]
    [SerializeField] List<ColorBarStep> colorSteps;
    [SerializeField] BarEffectType firstStepEeffectType;
    [SerializeField] float effectTransitionDelay;

    void Awake() {
        // order color steps, only if there are more than one color defined
        if (this.colorSteps != null && this.colorSteps.Count > 1)
        {
            this.colorSteps.Sort((a, b) => (a.toRange < b.toRange) ? -1 : (a.toRange > b.toRange) ? 1 : 0);
        }
        bar.minValue = 0; bar.maxValue = 1; // manage var using percent values
    }

    public void UpdateBar(float currentValue, float maxValue) 
    {
        float currentPercent = currentValue / maxValue;
        bar.value = currentPercent;

        if (this.displayText != null) {
            this.displayText.text = string.Format("{0}/{1}", (int)currentValue, (int)maxValue);
        }    

        float currentLerpPercent = 0;

        for (int index = 0, max = this.colorSteps.Count; index < max; ++index) {
            if (index == 0 && currentPercent < this.colorSteps[index].toRange)
            {
                bar.targetGraphic.color = this.colorSteps[index].color;

                if (effectTween == null) {
                    if (this.firstStepEeffectType == BarEffectType.Blink) {
                        StartBlinkEffect();
                    }
                    else if (colorSteps.Count > index+1) { 
                        StartColorEffect(this.colorSteps[index+1].color);
                    }
                }
                break;
            }
            else {

                if (effectTween != null) {
                    effectTween.Kill();
                    effectTween = null;
                }

                if (index == max - 1 && currentPercent >= this.colorSteps[index].toRange) {
                    bar.targetGraphic.color = this.colorSteps[index].color;
                }
                else if (currentPercent >= this.colorSteps[index].toRange && index+1 < max) {
                    currentLerpPercent = (currentValue - this.colorSteps[index].toRange) / (this.colorSteps[index + 1].toRange - this.colorSteps[index].toRange);
                    bar.targetGraphic.color = Color.Lerp(this.colorSteps[index].color, this.colorSteps[index + 1].color, currentLerpPercent);
                }
            }
        }
    }

    public void ShowBar(bool state) 
    {
        gameObject.SetActive (state);
    }

    Tweener effectTween;
    void StartBlinkEffect() {
        effectTween = this.bar.targetGraphic.DOFade(0, this.effectTransitionDelay).SetLoops(-1, LoopType.Yoyo);
    }

    void StartColorEffect(Color color) {
        effectTween = this.bar.targetGraphic.DOColor(color, this.effectTransitionDelay).SetLoops(-1, LoopType.Yoyo);
    }
}
