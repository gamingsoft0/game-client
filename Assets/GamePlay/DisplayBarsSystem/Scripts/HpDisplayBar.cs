﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class HpDisplayBar : DisplayBar {

    protected CharacterEntity m_Entity;
    public CharacterEntity Entity
    {
        set 
        {
            if (m_Entity != null) { RemoveEntity(); }
            m_Entity = value;
            ConfigureEntity();
        }
        get { return m_Entity; }
    }

    public bool m_ShowOnlyWhenChange;
    public bool m_HideWhenDead;
    public float m_SecondsToHideAgain = 1f;

    float m_SecondsToHide = 0f;

    void ConfigureEntity()
    {
        if (Entity == null)
        {
            return;
        }
        if (Entity.InteracterDM == null)
        {
            Debug.LogError("EntityDM is null", this);
            return;
        }
        entity_onHpChange(Entity, Entity.InteracterDM.CurrentHealthPoints, Entity.InteracterDM.Health);
        Entity.onHpChange += entity_onHpChange;

        base.ShowBar(!this.m_ShowOnlyWhenChange);
    }

    void RemoveEntity() 
    {
        if (Entity == null || Entity.InteracterDM == null) {
            return;
        }

        Entity.onHpChange -= entity_onHpChange;
    }

    void entity_onHpChange(CharacterEntity entity, float currentValue, float maxValue)
    {
        this.UpdateBar(currentValue, maxValue);
        if (currentValue <= 0) {
            this.StopAllCoroutines();
            if(m_HideWhenDead)
            {
                this.ShowBar(false);
            }
        }
        else {      
            UpdateSecondsToHide();
        }
    }

    void UpdateSecondsToHide() 
    {
        if (!m_ShowOnlyWhenChange) {
            return;
        }

        if (m_SecondsToHide > 0) {
            m_SecondsToHide += this.m_SecondsToHideAgain;
            return;
        }

        this.ShowBar(true);
        StartCoroutine(WaitAndHide(this.m_SecondsToHideAgain));
    }

    IEnumerator WaitAndHide(float seconds) 
    {
        m_SecondsToHide = seconds;
        while (m_SecondsToHide >= 0f) {
            yield return null;
            m_SecondsToHide -= Time.deltaTime;
        }

        this.ShowBar(false);
    }  
}
