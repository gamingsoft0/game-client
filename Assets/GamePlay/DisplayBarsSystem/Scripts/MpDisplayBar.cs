﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class MpDisplayBar : DisplayBar 
{
    protected CharacterEntity m_Entity;
    public CharacterEntity Entity
    {
        set 
        {
            if (m_Entity != null) { RemoveEntity(); }
            m_Entity = value;
            ConfigureEntity();
        }
        get { return m_Entity; }
    }

    void ConfigureEntity() {
        entity_onMpChange(Entity, Entity.InteracterDM.CurrentMagicPoints, Entity.InteracterDM.Wisdom);
        Entity.onMpChange += entity_onMpChange;
    }

    void entity_onMpChange(CharacterEntity entity, float currentValue, float maxValue) {
        this.UpdateBar(currentValue, maxValue);
    }

    void RemoveEntity() 
    {
        if (Entity == null || Entity.InteracterDM == null) {
            return;
        }

        Entity.onHpChange -= entity_onMpChange;
    }
}
