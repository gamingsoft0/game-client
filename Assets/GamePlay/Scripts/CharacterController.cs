﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// <para>Player character controller.</para>
/// <para>Part of the Third Person Controller, but adapted to be used in 2D environments</para>
/// <para>Also, movement is given by rigidbody instead of animator's root motion</para>
/// <author>Raúl Ibarra Aranda</author>
/// </summary>
public class CharacterController : MonoBehaviour
{
    [System.Serializable]
    public class MovementConfig
    {
        public float JumpPower = 12f;
        [Range(1f, 4f)] public float GravityMultiplier = 2f;
        [Range(1f, 10f)] public float MaxSpeed = 10f;
        [Range(1,10)] public int CombatTimeDuration = 3;
    }

    [Header("Character Asset Config")]
    [SerializeField]
    Transform m_CharacterTransform;
    [SerializeField] bool m_IsFacingRight;
    [Range(-180, 180)] [SerializeField] float m_RightFacingRotationAngle;
    [SerializeField] float m_RotationSpeedMultiplier = 2f;

    [Space(10)]
    [SerializeField]
    Transform m_GroundCheckPosition;
    [SerializeField] float m_GroundCheckDistance = 0.1f;

    [Space(10)]

    [SerializeField] MovementConfig m_MovementConfig;

    [System.NonSerialized] public bool GroundCheckEnabled = true;

    Rigidbody m_Rigidbody;
    CharacterAnimController m_AnimController;
    CharacterEntity m_CharacterEntity;

    public Transform AnimTransform { get; private set;}

    float m_OrigGroundCheckDistance;
    float m_ForwardAmount;

    bool m_IsGrounded = true;

    float currentCombatTime = 0;

    bool isAttacking = false;

    // Use this for initialization
    void Start()
    {
        m_CharacterEntity = GetComponentInParent<CharacterEntity> ();
        m_Rigidbody = GetComponentInChildren<Rigidbody>();
        m_AnimController = GetComponentInChildren<CharacterAnimController>();
        AnimTransform = m_AnimController.transform;

        m_Rigidbody.constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionZ;
        m_OrigGroundCheckDistance = m_GroundCheckDistance;

        m_AnimController.OnEndAttack = TriggerOnEndAttack;
        m_AnimController.OnEndSpell = TriggerOnEndSpell;
    }

    public System.Action OnEndAttack = delegate {};
    public System.Action<int> OnEndSpell = delegate {}; // carries spell id

    public void TriggerOnEndAttack()
    {
        OnEndAttack();
    }

    void TriggerOnEndSpell (int spellId)
    {
        OnEndSpell (spellId);
    }

    public void SetMovementConfig(MovementConfig config)
    {
        m_MovementConfig = config;
    }

    #region Movement

    public void Move(Vector3 move, bool jump)
    {
        // convert move vector to move direction
        if (move.magnitude > 1f) move.Normalize();

        CheckGroundStatus();

        m_ForwardAmount = CanMove ? move.x : 0f;

        // control and velocity handling is different when grounded and airborne:
        if (m_IsGrounded)
        {
            HandleGroundedMovement(jump);
        }
        else
        {
            HandleAirborneMovement();
        }

        HandleMovement();
        HandleRotation();
        UpdateCombatTime();
    }

    public bool CanMove
    {
        get
        {  
            if(m_AnimController == null)
            {
                return true;
            }
            return (!m_IsGrounded || !m_AnimController.IsBusy);
        }
    }

    public bool IsBusy{ get { return isAttacking || m_AnimController.IsInTransition; }}

    void HandleMovement()
    {
        if(!CanMove)
        {
            m_AnimController.SetSpeedPercent(0);
            m_AnimController.SetHeightVelocity(m_Rigidbody.velocity.y);
            m_Rigidbody.velocity = Vector3.zero;
            return;
        }

        if(m_Rigidbody == null)
        {
            return;
        }

        if (Time.deltaTime > 0)
        {
            float speedValue = m_ForwardAmount * m_MovementConfig.MaxSpeed;
            float speedPercent = speedValue / m_MovementConfig.MaxSpeed;

            Vector3 v = (Vector3.right * speedValue * Time.deltaTime);
            m_Rigidbody.MovePosition(m_Rigidbody.position + v);
            m_AnimController.SetSpeedPercent(Mathf.Abs(speedPercent));
            m_AnimController.SetHeightVelocity(m_Rigidbody.velocity.y);
        }
    }

    void HandleRotation()
    {
        m_IsFacingRight = m_ForwardAmount > 0 ? true : m_ForwardAmount < 0 ? false : m_IsFacingRight;

        float rotationToSet = m_IsFacingRight ? m_RightFacingRotationAngle : m_RightFacingRotationAngle - 180;

        if (Mathf.Approximately(m_CharacterTransform.localRotation.eulerAngles.y - rotationToSet, 0))
        {
            return;
        }

        m_CharacterTransform.localRotation = Quaternion.Lerp(m_CharacterTransform.localRotation, Quaternion.Euler(Vector3.up * rotationToSet), Time.deltaTime * m_RotationSpeedMultiplier);
    }

    /// <summary>
    /// Rotate the specified directionValue
    /// </summary>
    /// <param name="directionValue">Direction value. 1 means RIGHT, otherwise, LEFT is assumed</param>
    public void Rotate(int directionValue)
    {
        float rotationToSet = directionValue == 1 ? m_RightFacingRotationAngle : m_RightFacingRotationAngle - 180;
        m_CharacterTransform.localRotation = Quaternion.Euler(Vector3.up * rotationToSet);
    }

    void HandleAirborneMovement()
    {

        if (m_Rigidbody == null) 
        {
            return; // avoind handling logic because rigidbody is null
        }

        // apply extra gravity from multiplier:
        Vector3 extraGravityForce = (Physics.gravity * m_MovementConfig.GravityMultiplier) - Physics.gravity;
        m_Rigidbody.AddForce(extraGravityForce);

        m_GroundCheckDistance = m_Rigidbody.velocity.y < 0 ? m_OrigGroundCheckDistance : 0.01f;
    }

    void HandleGroundedMovement(bool jump)
    {
        // check whether conditions are right to allow a jump:
        if (jump)
        {
            // jump!
            m_Rigidbody.velocity = new Vector3(m_Rigidbody.velocity.x, m_MovementConfig.JumpPower, m_Rigidbody.velocity.z);
            m_AnimController.SetGroundedState(true);
            m_IsGrounded = false;
            m_GroundCheckDistance = 0.1f;
        }
    }

    void CheckGroundStatus()
    {
        if(!GroundCheckEnabled)
        {
            m_IsGrounded = true;
        }
        else
        {
            RaycastHit hitInfo;
            #if UNITY_EDITOR
            Debug.DrawLine(m_GroundCheckPosition.position + (Vector3.up * 0.1f), m_GroundCheckPosition.position + (Vector3.up * 0.1f) + (Vector3.down * m_GroundCheckDistance));
            #endif
            if (Physics.Raycast(m_GroundCheckPosition.position + (Vector3.up * 0.1f), Vector3.down, out hitInfo, m_GroundCheckDistance))
            {
                m_IsGrounded = true;
            }
            else
            {
                m_IsGrounded = false;
            }
        }

        if(m_AnimController != null)
        {
            m_AnimController.SetGroundedState(m_IsGrounded);
        }
    }

    #endregion End Movement

    #region Action

    /// <summary>
    /// <para>Handles combat time</para>
    /// <para>reduces one point per second</para>
    /// </summary>
    void UpdateCombatTime()
    {
        if(m_AnimController == null)
        {   // Sanity check added because unity's execution order and physics
            return;
        }

        currentCombatTime = Mathf.Lerp(currentCombatTime,0,Time.deltaTime * 1);
        m_AnimController.SetCombatState(currentCombatTime > 0.1f);
    }

    public void Defend(bool state){
        m_AnimController.SetDefendingState(state);
    }

    public void TriggerMeleeAttack()
    {
        if( !isAttacking && !m_AnimController.IsBusy)
        {
            currentCombatTime = m_MovementConfig.CombatTimeDuration;
            m_AnimController.MeleeAttack();
            TriggerAudioByType (ActionType.Attack);
            StartCoroutine(WaitForAttackCoroutine());
        }
    }

    IEnumerator WaitForAttackCoroutine()
    {
        isAttacking = true;
        yield return new WaitForSeconds(0.1f);
        while(m_AnimController.IsInTransition)
        {   // wait for enter into busy state
            yield return null;
        }
        while(m_AnimController.IsBusy)
        {   // wait for exit from busy state
            yield return null;
        }
        while(m_AnimController.IsInTransition)
        {   // wait for fully enter to next state
            yield return null;
        }
        yield return null;
        isAttacking = false;
    }

    public void TriggerRangedAttack()
    {
        if(!m_AnimController.IsBusy)
        {
            m_AnimController.RangeAttack();
        }
    }

    /// <summary>
    /// <para>Casts a spell.</para>
    /// <para>This method uses a custom spell cost of 10. in a future release, this value must come from a spell definition</para>
    /// </summary>
    /// <param name="spellIndex">Spell index.</param>
    public void CastSkill(int spellIndex)
    {
        if( !isAttacking && !m_AnimController.IsBusy && m_CharacterEntity.MagicPoints >= 5)
        {
            m_AnimController.CastSkill(spellIndex);
            TriggerAudioByType (ActionType.Skill);
            m_CharacterEntity.ReceiveMPHeal (new AttackParams{ damage = -5});
        }
    }

    public void ReceiveDamage()
    {
        m_AnimController.ReceiveDamage ();
    }

    public void Die()
    {
        m_AnimController.Die ();
        if (m_IsGrounded) 
        {
            m_Rigidbody.isKinematic = true;
            m_Rigidbody.useGravity = false;
            GetComponent<Collider> ().isTrigger = true;
        }
    }

    void TriggerAudioByType(ActionType actionType)
    {
        AudioClip clipToPlay = null;
        switch (actionType)
        {
        case ActionType.Attack:
            clipToPlay = m_CharacterEntity.InteracterDM.AttackAudio.GetRandom ();
            break;
        case ActionType.Skill:
            clipToPlay = m_CharacterEntity.InteracterDM.SkillsAudio.GetRandom ();
            break;
        }
        if (clipToPlay != null)
        {
            AudioManager.Instance.PlayClip (clipToPlay);
        }
    }

    #endregion End Action

    void OnDestroy()
    {
        if (m_AnimController != null) 
        {
            m_AnimController.OnEndAttack = null;
        }
    }
}
