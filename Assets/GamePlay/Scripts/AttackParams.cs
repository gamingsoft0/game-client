﻿public class AttackParams
{
    public float damage;
    public AttackNature attackType;
    public CharacterEntity executioner;
    public InteracterDM interacterDM;
    public ElementType element;
    public bool ignoreArmor;
    public bool ignoreShield;
    public bool undodgeable;
}