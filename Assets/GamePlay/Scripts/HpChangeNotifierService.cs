﻿using UnityEngine;
using System.Collections;

public class HpChangeNotifierService : Singleton<HpChangeNotifierService>
{
    public HpChangeNotifier hpChangeNotifierPrefab;
    public Camera facingCamera;

    public void NotifyDamage(CharacterEntity applier, CharacterEntity target, float damage, bool isCritical)
    {
        HpChangeNotifier currentNotification = GetNewHpChangeNotifier();

        currentNotification.Configure(applier, target, damage, HpChangeNotifier.HpChangeType.Damage, isCritical,facingCamera);
        StartCoroutine(currentNotification.Show());
    }

    public void NotifyHeal(CharacterEntity applier, CharacterEntity target, float healAmmount)
    {
        HpChangeNotifier currentNotification = GetNewHpChangeNotifier();

        currentNotification.Configure(applier, target, healAmmount, HpChangeNotifier.HpChangeType.Heal,false,this.facingCamera);
        StartCoroutine(currentNotification.Show());
    }

    private HpChangeNotifier GetNewHpChangeNotifier()
    {
        GameObject newNotification = Object.Instantiate(hpChangeNotifierPrefab.gameObject);
        newNotification.transform.SetParent(this.transform,false);
        HpChangeNotifier currentNotification = newNotification.GetComponent<HpChangeNotifier>();
        return currentNotification;
    }

    public override void OnDestroy ()
    {
        m_Instance = null; // this singleton tan be destroyed safely
    }
}