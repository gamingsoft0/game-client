﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangeDetector : MonoBehaviour 
{
    public const string ALLY_TAG = "Player";
    public const string ENEMY_TAG = "Enemy";

    EntityType team;
    SphereCollider m_SphereCollider;

    public event System.Action<CharacterEntity> OnEnemyDetected = delegate {};
    public event System.Action<CharacterEntity> OnEnemyLost = delegate {};

    private List<CharacterEntity> m_EntitiesInRange = new List<CharacterEntity>();
    public List<CharacterEntity> EntitiesInRange { get { RemoveDeadEntities(); return m_EntitiesInRange; } }

    public CharacterEntity GetClosesEntity()
    {
        RemoveDeadEntities();

        if(m_EntitiesInRange.Count == 0)
        {
            return null;
        }

        if(m_EntitiesInRange.Count == 1)
        {
            return m_EntitiesInRange[0];
        }

        // Do this only if there is more than one enemy to check at
        float closestDistance = float.MaxValue;
        float currentDistance = 0;
        CharacterEntity toReturn = null;
        for(int i = 0; i < m_EntitiesInRange.Count; ++i)
        {
            currentDistance = (transform.position - m_EntitiesInRange[i].position).sqrMagnitude;
            if(currentDistance < closestDistance)
            {
                closestDistance = currentDistance;
                toReturn = m_EntitiesInRange[i];
            }
        }
        return toReturn;
    }

    public void RemoveDeadEntities()
    {
        m_EntitiesInRange.RemoveAll(w => w.IsDead);
    }

    public bool IsInRange(CharacterEntity entity)
    {
        return m_EntitiesInRange.IndexOf(entity) != -1;
    }

    public void Config(EntityType team, float radius)
    {        
        this.team = team;
        m_SphereCollider = gameObject.AddComponent<SphereCollider>();
        m_SphereCollider.radius = radius;
        m_SphereCollider.isTrigger = true;
    }

    bool IsEnemyTag(string otherTag)
    {
        return ((team == EntityType.Character && ENEMY_TAG.Equals(otherTag)) || (team == EntityType.Enemy && ALLY_TAG.Equals(otherTag) ));
    }

    void OnTriggerEnter(Collider other)
    {
        if(!IsEnemyTag(other.tag))
        {
            return;
        }
        CharacterEntity entity = other.GetComponentInParent<CharacterEntity>();
        if(entity != null)
        {
            OnEnemyDetected(entity);
            m_EntitiesInRange.Add(entity);
        }
    }

    void OnTriggerExit(Collider other)
    {
        if(!IsEnemyTag(other.tag))
        {
            return;
        }

        CharacterEntity entity = other.GetComponentInParent<CharacterEntity>();
        if(entity)
        {
            OnEnemyLost(entity);
            m_EntitiesInRange.Remove(entity);
        }
    }
}
