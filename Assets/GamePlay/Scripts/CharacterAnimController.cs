﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// <para>Character animation controller.</para>
/// <para>Handles animator states with common state names</para>
/// <author>Raúl Ibarra Aranda</author>
/// </summary>
[RequireComponent(typeof(Animator))]
public class CharacterAnimController : MonoBehaviour 
{
    [SerializeField] int m_MaxMeleeAttackAnimIndex;
    [SerializeField] int m_MaxRangeAttackAnimIndex;
    [SerializeField] int m_MaxReceiveDamageAnimIndex;
    [SerializeField] int m_MaxDieAnimIndex;

    const string BUSY_TAG = "busy";

    private Animator m_Animator;

    const string PARAM_SPEED = "speed";
    const string PARAM_GROUNDED = "grounded";
    const string PARAM_HEIGHT_VELOCITY = "height_velocity";
    const string PARAM_IN_COMBAT = "in_combat";
    const string PARAM_MELEE_ATTACK = "melee_attack";
    const string PARAM_MELEE_ATTACK_INDEX = "melee_attack_index";
    const string PARAM_RANGE_ATTACK = "range_attack";
    const string PARAM_RANGE_ATTACK_INDEX = "range_attack_index";
    const string PARAM_MAGIC_ATTACK = "magic_attack";
    const string PARAM_MAGIC_ATTACK_INDEX = "magic_attack_index";
    const string PARAM_DEFEND = "defend";
    const string PARAM_RECEIVE_DAMAGE = "receive_damage";
    const string PARAM_RECEIVE_DAMAGE_INDEX = "receive_damage_index";
    const string PARAM_DIE = "die";
    const string PARAM_DIE_INDEX = "die_index";

    #region Anim to Hash Logic
    private static Dictionary<string, int> m_AnimHash;
    private static bool m_AnimHashInitialized;

    /// <summary>
    /// Initializes animation hashes for all animator parameters.
    /// </summary>
    private static void InitializeAnimHash()
    {
		m_AnimHash = new Dictionary<string, int>();
		m_AnimHash[PARAM_SPEED] = Animator.StringToHash(PARAM_SPEED);
        m_AnimHash[PARAM_GROUNDED] = Animator.StringToHash(PARAM_GROUNDED);
        m_AnimHash[PARAM_HEIGHT_VELOCITY] = Animator.StringToHash(PARAM_HEIGHT_VELOCITY);
        m_AnimHash[PARAM_IN_COMBAT] = Animator.StringToHash(PARAM_IN_COMBAT);
        m_AnimHash[PARAM_MELEE_ATTACK] = Animator.StringToHash(PARAM_MELEE_ATTACK);
        m_AnimHash[PARAM_MELEE_ATTACK_INDEX] = Animator.StringToHash(PARAM_MELEE_ATTACK_INDEX);
        m_AnimHash[PARAM_RANGE_ATTACK] = Animator.StringToHash(PARAM_RANGE_ATTACK);
        m_AnimHash[PARAM_RANGE_ATTACK_INDEX] = Animator.StringToHash(PARAM_RANGE_ATTACK_INDEX);
        m_AnimHash[PARAM_MAGIC_ATTACK] = Animator.StringToHash(PARAM_MAGIC_ATTACK);
        m_AnimHash[PARAM_MAGIC_ATTACK_INDEX] = Animator.StringToHash(PARAM_MAGIC_ATTACK_INDEX);
        m_AnimHash[PARAM_DEFEND] = Animator.StringToHash(PARAM_DEFEND);
        m_AnimHash[PARAM_RECEIVE_DAMAGE] = Animator.StringToHash(PARAM_RECEIVE_DAMAGE);
        m_AnimHash[PARAM_RECEIVE_DAMAGE_INDEX] = Animator.StringToHash(PARAM_RECEIVE_DAMAGE_INDEX);
        m_AnimHash[PARAM_DIE] = Animator.StringToHash(PARAM_DIE);
        m_AnimHash[PARAM_DIE_INDEX] = Animator.StringToHash(PARAM_DIE_INDEX);
        m_AnimHashInitialized = true;
    }
    #endregion end Anim to Hash Logic

    private int m_CurrentMeleeAttackIndex;
    private int m_CurrentRangeAttackIndex;
    private int m_CurrentReceiveDamageIndex;

    public void TriggerEndAttack()
    {
        OnEndAttack();
    }

    public void TriggerEndSpell()
    {
        OnEndSpell ((int)m_Animator.GetFloat(PARAM_MAGIC_ATTACK_INDEX));
    }

    public System.Action OnEndAttack = delegate {};
    public System.Action<int> OnEndSpell = delegate {}; // carries spell id

    void Awake()
    {
        m_Animator = GetComponent<Animator>();
    }

    public bool IsBusy 
    {
        get
        { 
            return m_Animator.GetCurrentAnimatorStateInfo(0).IsTag(BUSY_TAG); 
        }
    }

    public bool IsInTransition{ get{ return m_Animator.IsInTransition(0); } }

	/// <summary>
	/// <para>Sets speed percentaje of the character. 0 Is Walking and 1 is Running</para>
	/// <para>Any value in between blends animations</para>
	/// </summary>
	/// <param name="speedPercent">Speed.</param>
	public void SetSpeedPercent(float speedPercent)
    {
        if (!m_AnimHashInitialized) { InitializeAnimHash(); }
        m_Animator.SetFloat(m_AnimHash[PARAM_SPEED],speedPercent);
    }

    public void SetGroundedState(bool state)
    {
        if (!m_AnimHashInitialized) { InitializeAnimHash(); }
        m_Animator.SetBool(m_AnimHash[PARAM_GROUNDED], state);
    }

    public void SetHeightVelocity(float velocity)
    {
        if (!m_AnimHashInitialized) { InitializeAnimHash(); }
        m_Animator.SetFloat(m_AnimHash[PARAM_HEIGHT_VELOCITY],velocity);
    }

    public void SetCombatState(bool state)
    {
        if (!m_AnimHashInitialized){InitializeAnimHash();}
        m_Animator.SetFloat(m_AnimHash[PARAM_IN_COMBAT], state ? 1f : 0f);
    }

    public void MeleeAttack()
    {
        if (!m_AnimHashInitialized) { InitializeAnimHash(); }
        m_Animator.SetFloat(m_AnimHash[PARAM_MELEE_ATTACK_INDEX], m_CurrentMeleeAttackIndex);
        m_Animator.SetTrigger(m_AnimHash[PARAM_MELEE_ATTACK]);
        m_CurrentMeleeAttackIndex = m_CurrentMeleeAttackIndex < m_MaxMeleeAttackAnimIndex ? m_CurrentMeleeAttackIndex+1 : 0;
    }

    public void RangeAttack()
    {
        if (!m_AnimHashInitialized) { InitializeAnimHash(); }
		m_Animator.SetFloat(m_AnimHash[PARAM_RANGE_ATTACK_INDEX], m_CurrentRangeAttackIndex);
        m_Animator.SetTrigger(m_AnimHash[PARAM_RANGE_ATTACK]);		
		m_CurrentRangeAttackIndex = m_CurrentRangeAttackIndex < m_MaxRangeAttackAnimIndex ? m_CurrentRangeAttackIndex+1 : 0;
    }

	public void CastSkill(int spellIndex)
	{
        if (!m_AnimHashInitialized) { InitializeAnimHash(); }
        m_Animator.SetFloat(m_AnimHash[PARAM_MAGIC_ATTACK_INDEX], spellIndex);
        m_Animator.SetTrigger(m_AnimHash[PARAM_MAGIC_ATTACK]);
	}

    public void SetDefendingState(bool state)
    {
        if (!m_AnimHashInitialized) { InitializeAnimHash(); }
        m_Animator.SetBool(m_AnimHash[PARAM_DEFEND],state);
    }

    public bool GetDefendingState(){
        if (!m_AnimHashInitialized) { InitializeAnimHash(); }
        return m_Animator.GetBool(m_AnimHash[PARAM_DEFEND]);
    }

    public void ReceiveDamage()
    {
        if (!m_AnimHashInitialized) { InitializeAnimHash(); }
        m_Animator.SetFloat(m_AnimHash[PARAM_RECEIVE_DAMAGE_INDEX],m_CurrentReceiveDamageIndex);
        m_Animator.SetTrigger(m_AnimHash[PARAM_RECEIVE_DAMAGE]);
        m_CurrentReceiveDamageIndex = m_CurrentReceiveDamageIndex < m_MaxReceiveDamageAnimIndex ? m_CurrentReceiveDamageIndex+1 : 0;
    }

    public void Die()
    {
        if (!m_AnimHashInitialized) { InitializeAnimHash(); }
        m_Animator.SetFloat(m_AnimHash[PARAM_DIE_INDEX],Random.Range(0,m_MaxDieAnimIndex -1));
        m_Animator.SetTrigger(m_AnimHash[PARAM_DIE]);
    }
}
