﻿public enum AttackNature {
    None = 0,
    Physic = 1,
    Magic = 2,
}