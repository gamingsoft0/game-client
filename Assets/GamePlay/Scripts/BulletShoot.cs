﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// <para>This does an attack by trowing an sphere collider facing in the same direction as the executioner</para>
/// <para>If this enters in contact with an enemy, then applies the specified damage</para>
/// </summary>
public class BulletShoot : MonoBehaviour 
{
    public float speed = 20f;
    public Vector3 relativeDirection = Vector3.forward;

    private int playerAttack = 5;
    public int totalDamage = 0;
    public int variance = 15;
    public bool stopAtFirstContact = true;
    CharacterEntity shooter;

    string enemyTag;

    public void Init(CharacterEntity owner, AttackNature attackType)
    {
        enemyTag = owner.Type == EntityType.Character ? RangeDetector.ENEMY_TAG : RangeDetector.ALLY_TAG;

        this.playerAttack = attackType == AttackNature.Physic ? owner.AttackPoints : owner.Wisdom;
        this.shooter = owner;
        int varMin = 100 - variance;
        int varMax = 100 + variance;
        totalDamage = playerAttack * Random.Range(varMin, varMax) / 100;
    }

    void Update()
    {
        Vector3 absoluteDirection = transform.rotation * relativeDirection;
        transform.position += absoluteDirection * speed * Time.deltaTime;
    }

    void OnTriggerEnter(Collider other)
    {
        if(!other.CompareTag(enemyTag))
        {
            return; // invalid target
        }

        CharacterEntity target = other.GetComponentInParent<CharacterEntity>();
        if(target == null)
        {
            return; // invalid target
        }

        AttackParams attackParams = new AttackParams
        {
            damage = totalDamage,
            executioner = shooter,
            attackType = AttackNature.Physic,
            element = ElementType.None,
            interacterDM = shooter.InteracterDM
        };

        target.ReceiveDamage(attackParams);

        if (stopAtFirstContact) 
        {
            GetComponentInChildren<Collider> ().enabled = false;
            Object.Destroy(this.gameObject,1f);
        }
    }
}
