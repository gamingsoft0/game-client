﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour 
{
    const float ATTACK_DISTANCE_MULTIPLIER = 5.5f;
    const float SECONDS_TO_MAX_SPEED = 5f;

    public CharacterController CharacterController;

    private CharacterEntity m_Entity;
    private CharacterEntity m_Enemy;
    private RangeDetector m_Detector;
    private Vector3 m_Move;
    private float m_AttackDistance;
    private float m_CooldownTimer;

    bool IsInAttackRange
    {
        get
        {
            return m_Entity != null && Mathf.Abs(m_Enemy.position.x - m_Entity.position.x) <= m_AttackDistance;
        }
    }

    bool CanAttack 
    {
        get 
        {
            return CharacterController.CanMove && !CharacterController.IsBusy && IsInAttackRange && m_CooldownTimer >= m_Entity.InteracterDM.AttackCooldown;
        }
    }

    private void Start()
    {
        if(CharacterController == null)
        {
            CharacterController = GetComponentInChildren<CharacterController>();
        }
    }

    public void Config(CharacterEntity entity)
    {
        this.m_Entity = entity;

        GameObject detectorGo = new GameObject(typeof(RangeDetector).Name);
        detectorGo.transform.SetParent(CharacterController.transform,false);
        m_Detector = detectorGo.AddComponent<RangeDetector>();
        m_Detector.Config(EntityType.Enemy, entity.InteracterDM.DetectionRange);
        m_AttackDistance = entity.EntityCollider.radius * ATTACK_DISTANCE_MULTIPLIER; // arbitraty number to detect body range plus arms
        CharacterController.GroundCheckEnabled = false; // disable ground check for enemies
        entity.onHpChange += CheckEnemyDeath;
    }

    void CheckEnemyDeath (CharacterEntity entity, float currentValue, float maxValue)
    {
        if(entity.IsDead)
        {
            GameProgress.Instance.AddEnemyDefeated();
            if (m_Enemy != null && m_Enemy.Type == EntityType.Character && m_Entity.IsBoss) 
            {
                GameManager.Instance.RemoveCameraTarget (this.m_Entity.MovementComponent.transform);
            }
        }
    }

    void Update()
    {
        if ( GameManager.Instance.IsGameEnded || this.m_Entity.IsDead) 
        {
            return; // Do nothing. entity already dead
        }

        UpdateCooldown ();

        if(GameManager.Instance.IsGameEnded)
        {
            m_Enemy = null;
            Move(Vector3.zero);
        }

        if(m_Enemy == null)
        {
            m_Enemy = m_Detector.GetClosesEntity();

            if (m_Enemy != null && this.m_Enemy.Type == EntityType.Character && this.m_Entity.IsBoss) {
                // add enemy to player's camera targets
                GameManager.Instance.AddCameraTarget(this.m_Entity.MovementComponent.transform,1);
            }
        }

        if (m_Enemy == null) {   // there is no enemy in sight. wait for next update
            Move (Vector3.zero); 
            return; 
        }

        if (!m_Detector.IsInRange(m_Enemy)) 
        {
            if (m_Enemy.Type == EntityType.Character && this.m_Entity.IsBoss) 
            {
                // remove this enemy from player's camera targets
                GameManager.Instance.RemoveCameraTarget(this.m_Entity.MovementComponent.transform);
            }

            m_Enemy = null; // wait for the next frame to chase a new enemy
            Move(Vector3.zero); 

            return;
        }

        if(m_Enemy.IsDead) 
        {   // Skip attacking enemy if is already dead
            m_Enemy = null;
            Move(Vector3.zero);
            return;
        }            

        if(CanAttack)
        {            
            FaceEnemy();
            CharacterController.TriggerMeleeAttack();
            return;
        }
        else if(CharacterController.CanMove && !CharacterController.IsBusy)
        {   // chase enemy            
            // check if enemy is at right or left to this entity
            if(m_Enemy.position.x > m_Entity.position.x) // at right
            {                
                Move(Vector3.right);
            }
            else
            {
                Move(Vector3.left);
            }
        }
        else
        {
            Move(Vector3.zero); // stop movement
        }
    }

    void Move(Vector3 direction)
    {
        m_Move = Vector3.Lerp(m_Move, direction, Time.deltaTime * SECONDS_TO_MAX_SPEED);
        CharacterController.Move(m_Move, false);
    }

    void FaceEnemy()
    {
        if(m_Enemy.position.x > m_Entity.position.x) // rotate right
        {
            CharacterController.Rotate(1);
        }
        else // rotate left
        {
            CharacterController.Rotate(-1);
        }
    }

    void UpdateCooldown()
    {
        if (m_CooldownTimer < m_Entity.InteracterDM.AttackCooldown) 
        {
            m_CooldownTimer += Time.deltaTime;
        }
    }

    void OnDestroy()
    {
        m_Entity.onHpChange -= CheckEnemyDeath;
    }
}
