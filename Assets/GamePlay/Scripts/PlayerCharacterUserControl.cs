﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

/// <summary>
/// <para>Player character user control.</para>
/// <para>Part of the ThirdPersonUserControl, adapted to this game </para>
/// <author>Raúl Ibarra Aranda</author>
/// </summary>
public class PlayerCharacterUserControl : MonoBehaviour 
{
    public CharacterController CharacterController;

    private Vector3 m_Move;
    private bool m_Jump;
    private bool m_Defending;

    private void Start()
    {
        if(CharacterController == null)
        {
            CharacterController = GetComponentInChildren<CharacterController>();
        }
    }

    private void Update()
    {
        if(GameManager.Instance.IsGameEnded || Time.timeScale < 1)
        {
            return; // ignore update. game already ended
        }

        if (!m_Jump)
        {
            m_Jump = CrossPlatformInputManager.GetButtonDown("Jump");
        }
        if (CrossPlatformInputManager.GetButtonDown ("Fire1"))
        {
            CharacterController.TriggerMeleeAttack ();
        }

        if (CrossPlatformInputManager.GetButtonDown ("Fire2") && GameProgress.Instance.IsPowerUpCollected(PowerUpType.Spell01))
        {
            CharacterController.CastSkill (0);
        }

        if (!m_Defending && CrossPlatformInputManager.GetButtonDown ("Fire3")) {
            m_Defending = true;
            CharacterController.Defend (m_Defending);
        } else if (m_Defending && CrossPlatformInputManager.GetButtonUp ("Fire3")) {
            m_Defending = false;
            CharacterController.Defend (m_Defending);
        }
    }

    private void FixedUpdate()
    {
        if(GameManager.Instance.IsGameEnded)
        {
            CharacterController.Move(Vector3.zero,false);
            return; // ignore fixed update. game already ended
        }

        // read inputs
        float h = CrossPlatformInputManager.GetAxis("Horizontal");
        //float v = CrossPlatformInputManager.GetAxis("Vertical");
        m_Move = /*v * Vector3.forward +*/ h * Vector3.right;
        CharacterController.Move(m_Move,m_Jump);
        m_Jump = false; // var re-initialization
    }
}
