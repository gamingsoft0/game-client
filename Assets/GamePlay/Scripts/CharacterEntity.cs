﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class CharacterEntity : MonoBehaviour
{
    public delegate void OnStatChangeDelegate(CharacterEntity entity, float currentValue, float maxValue);
    public event OnStatChangeDelegate onHpChange = delegate { };
    public event OnStatChangeDelegate onMpChange = delegate { };

    #region Getters / Setters

    public bool IsDead { get { return (this.InteracterDM.CurrentHealthPoints <= 0); } }
    public bool IsInvincible { get; set; }
    public Vector3 position { get { return this.MovementComponent.transform.position; } }

    public int AttackPoints{ get{ return this.InteracterDM.Strength; }}
    public int MagicPoints{ get { return Mathf.CeilToInt (this.InteracterDM.CurrentMagicPoints); } }
    public int DefensePoints { get{ return this.InteracterDM.Defense; } }
    public int MagicDefensePoints { get{ return this.InteracterDM.MagicDefense; } }
    public int AgilityPoints { get{ return this.InteracterDM.Agility; }}
    public int HealthPoints{ get{ return Mathf.CeilToInt (this.InteracterDM.CurrentHealthPoints); } }
    public float HealthPercent {get{ return this.InteracterDM.CurrentHealthPoints / this.InteracterDM.Health; }}
    public int Wisdom{ get{ return Mathf.CeilToInt (this.InteracterDM.Wisdom); }}

    #endregion Getters / Setters

    public string CharacterId = string.Empty;
    public EntityType Type;
    public bool IsBoss;
    public CapsuleCollider EntityCollider;
    public CharacterController MovementComponent;
    [System.NonSerialized] public InteracterDM InteracterDM;
    public float EntityHeight {get; private set;}
    public Vector3 EntityCenter {get; private set;}
    public float EntityRadius {get; private set;}

    private GameObject prefabInstance;

    public void ReceiveHeal(AttackParams attackParams) 
    {
        this.InteracterDM.AddLifePoints(attackParams.damage);
        HpChangeNotifierService.Instance.NotifyHeal(attackParams.executioner, this, attackParams.damage);
        this.onHpChange(this,this.InteracterDM.CurrentHealthPoints, this.InteracterDM.Health);
        //Debug.Log(string.Format("Heal Received from : {0}",(attackParams.executioner != null ? attackParams.executioner.name : "unknown")));
    }

    public void ReceiveMPHeal(AttackParams attackParams) 
    { 
        this.InteracterDM.AddMagicPoints(attackParams.damage);
        this.onMpChange(this,this.InteracterDM.CurrentMagicPoints, this.InteracterDM.Wisdom);
        //Debug.Log(string.Format("MP Heal received from : {0}", attackParams.executioner != null ? attackParams.executioner.name : "unknown"));
    }

    public void ReceiveDamage(AttackParams attackParams)
    {
        if (IsDead || IsInvincible) { return; }
        if (!attackParams.undodgeable) 
        {
            // check if attack can be evaded, based on this site: pathofexile.gamepedia.com/evasion
            float chanceToEvade = (1 - attackParams.interacterDM.HitChance) / (attackParams.interacterDM.HitChance + Mathf.Pow(this.InteracterDM.Agility/4,0.8f));
            if (Random.value < chanceToEvade){
                ManageEvadedAttack(attackParams.executioner);
                return;
            }
        }

        float damage = attackParams.damage;

        if (!attackParams.ignoreArmor) {
            float defensePoints = (attackParams.attackType == AttackNature.Physic) ? this.DefensePoints : this.MagicDefensePoints;
            const float armorDamageMultiplier = 10f;
            float armorReduction = (defensePoints / (defensePoints + (armorDamageMultiplier * damage) ));

            if (armorReduction > damage * 0.9f) {armorReduction = damage * 0.9f;}
            damage -= armorReduction;
        }

        //Calculate Element Resistance  
        if (this.InteracterDM.ElementalResist != null) {
            ElementalResistData elementResistance = this.InteracterDM.ElementalResist.FirstOrDefault(w => w.element == attackParams.element);
            if (elementResistance != null)
            {
                damage *= elementResistance.resistance;
            }
        }

        // from this point, this character is going to be damaged and be affected by flinch
        bool flinch = false;

        this.InteracterDM.AddLifePoints(-damage);
        this.onHpChange(this,this.InteracterDM.CurrentHealthPoints, this.InteracterDM.Health);

        if (this.IsDead) 
        {
            this.MovementComponent.Die();
            if (this.Type == EntityType.Enemy) 
            {
                Object.Destroy (this.gameObject, 3f); // 3 seconds auto destroy for enemies
            }
        } 
        else if (flinch) 
        {
            this.MovementComponent.ReceiveDamage ();
        }

        HpChangeNotifierService.Instance.NotifyDamage(attackParams.executioner, this, damage, false);
    }

    public void ManageEvadedAttack (CharacterEntity interacterEntity)
    {
        Debug.Log(string.Format("{0} evaded attack from {1}",this.InteracterDM.Name,interacterEntity.InteracterDM.Id));
    }

    void Start()
    {
        this.InteracterDM = new InteracterDM (GameDataDB.Instance.GetCharacterDefinitionById(CharacterId));

        InstantiateSelectedPrefab();

        if (this.EntityCollider != null) 
        {
            this.EntityHeight = EntityCollider.height;
            this.EntityCenter = EntityCollider.center;
            this.EntityRadius = EntityCollider.radius;
        }

        MovementComponent.OnEndAttack = Handle_OnEndAttack;
        MovementComponent.OnEndSpell = Handle_OnEndSpell;
    }

    void Handle_OnEndAttack ()
    {
        BulletShoot bulletInstance = GameManager.Instance.GetBulletInstance();
        bulletInstance.transform.position = MovementComponent.AnimTransform.position + this.EntityCenter;
        bulletInstance.transform.LookAt(bulletInstance.transform.position + MovementComponent.AnimTransform.forward * 10f);
        bulletInstance.Init(this, AttackNature.Physic);
        Object.Destroy(bulletInstance.gameObject, 0.2f);
    }

    void Handle_OnEndSpell (int spellIndex)
    {
        BulletShoot bulletInstance = GameManager.Instance.GetSpellInstance(spellIndex);
        bulletInstance.transform.position = MovementComponent.AnimTransform.position + this.EntityCenter;
        bulletInstance.transform.LookAt(bulletInstance.transform.position + MovementComponent.AnimTransform.forward * 10f);
        bulletInstance.Init(this, AttackNature.Magic);
        Object.Destroy(bulletInstance.gameObject, 3f); // this is a projectile that can travell a larger distance
    }

    public void InstantiateSelectedPrefab()
    {
        if(InteracterDM == null)
        {
            Debug.LogErrorFormat(this,"Unable to instantiate Prefab. Invalid definition for id: {0}",CharacterId);
            return;
        }

        transform.DestroyChildren();

        prefabInstance = Object.Instantiate(InteracterDM.Prefab);
        prefabInstance.transform.localPosition = Vector3.zero;
        prefabInstance.transform.SetParent(transform, false);

        MovementComponent = prefabInstance.GetComponentInChildren<CharacterController>();
        EntityCollider = prefabInstance.GetComponentInChildren<CapsuleCollider>();

        MovementComponent.SetMovementConfig(InteracterDM.MovementConfig);

        if(this.Type == EntityType.Character)
        {
            PlayerCharacterUserControl control = GetComponent<PlayerCharacterUserControl>();
            if(control == null)
            {
                control = this.gameObject.AddComponent<PlayerCharacterUserControl>();
            }
            control.CharacterController = MovementComponent;

            prefabInstance.SetTag(RangeDetector.ALLY_TAG);

            if (Application.isPlaying)
            {
                this.ReceiveMPHeal (new AttackParams{ damage = -MagicPoints }); // start with zero mp. Only for this game
                GameManager.Instance.ConfigMainPlayer(this);
				GameManager.Instance.ConfigMainCameraTarget(prefabInstance.transform);
            }
        }
        else
        {
            prefabInstance.SetTag(RangeDetector.ENEMY_TAG);

            EnemyController control = GetComponent<EnemyController>();
            if(control == null)
            {
                control = gameObject.AddComponent<EnemyController>();
            }

            control.CharacterController = MovementComponent;
            control.Config(this);
        }
    }

    void OnDestroy()
    {
        MovementComponent.OnEndAttack = null;
    }
}

