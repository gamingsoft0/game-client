﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class HpChangeNotifier : MonoBehaviour
{
    public enum NotifierPosition {
        Ground,
        Center,
        Head
    }

    public enum HpChangeType {
        Damage,
        Heal
    }

    #region public vars

    public UnityEngine.UI.Text damageText;
    public NotifierPosition initialPositionType;
    [Range(0, 2)]
    public float initialPositionFactor = 1f;
    [Range(0, 2)]
    public float finalPositionFactor = 1f;
    public float timeToSelfDestruct = 2f;

    [Tooltip("Valid Format for use with String.Format method, applied to hpChange text")]
    public string hpChangeFormat = "N1";

    #endregion end public vars

    #region private vars

    private Vector3 initialPosition;
    private Vector3 finalPosition;
    private Camera facingCamera;  

    #endregion private vars

    public void Configure(CharacterEntity applier, CharacterEntity target, float rawChange, HpChangeType changeType, bool isCritic = false, Camera facingCamera = null)
    {
        #region notify text config

        this.damageText.text = (rawChange).ToString(hpChangeFormat);
        this.facingCamera = facingCamera;

        switch (changeType)
        {
        case HpChangeType.Damage:

            if (isCritic) {
                this.damageText.color = Color.red;
            }
            else {
                this.damageText.color = Color.white;
            }

            break;
        case HpChangeType.Heal:
            this.damageText.color = Color.green;
            break;
        default:
            break;
        }

        #endregion end notify text config

        #region position config

        this.initialPosition = target.position;

        if (initialPositionType == NotifierPosition.Head) {
            this.initialPosition.y += (target.EntityHeight * initialPositionFactor);
        }

        this.finalPosition = target.position;
        this.finalPosition.y += (target.EntityHeight * finalPositionFactor);

        #endregion end position config

        StartCoroutine(Show());
    }

    public IEnumerator Show() {
        this.transform.position = this.initialPosition;
        if (this.facingCamera != null) {

            //this.transform.rotation = Quaternion.LookRotation(facingCamera.transform.position - this.transform.position);
            this.transform.rotation = Quaternion.LookRotation(this.transform.position - facingCamera.transform.position);
            //this.transform.LookAt(this.facingCamera.transform);
        }

        Tweener positionTweener = this.transform.DOMove(this.finalPosition, timeToSelfDestruct);
        DG.Tweening.DOTween.ToAlpha(()=>this.damageText.color,x=>this.damageText.color=x,0,timeToSelfDestruct);

        yield return positionTweener.WaitForCompletion();

        Destroy(this.gameObject);
    }
}