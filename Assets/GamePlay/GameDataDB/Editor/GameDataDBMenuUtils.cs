using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;

public class GameDataDBMenuUtils
{
    public static void ShowCharactersDBEditor (string selectedId)
    {
        CharactersDBEditor editor = EditorWindow.GetWindow<CharactersDBEditor> ("Characters DB", true);

        editor.db = Selection.activeObject as GameDataLibrary;

        if (editor.db == null) { // try to get from instance
            editor.db = FindAndLoadDB<GameDataLibrary>();
        }

        if (editor.db == null) {
            editor.db = ScriptableObject.CreateInstance<GameDataLibrary> ();
            editor.db.characters = new List<EntityStats> ();
        }
        editor.SetInitialData (selectedId);
    }

    public static void ShowPowerUpsDBEditor(string selectedId)
    {
        PowerUpsDBEditor editor = EditorWindow.GetWindow<PowerUpsDBEditor>("PowerUps DB", true);
        editor.db = Selection.activeObject as GameDataLibrary;

        if(editor.db == null)
        {
            editor.db = FindAndLoadDB<GameDataLibrary>();
        }

        if(editor.db == null)
        {
            editor.db = ScriptableObject.CreateInstance<GameDataLibrary>();
            editor.db.powerUps = new List<PowerUpDefinition>();
        }

        editor.SetInitialData(selectedId);
    }

    public static void ShowGameDataDBEditor()
    {
        GameDataDBEditor editor = EditorWindow.GetWindow<GameDataDBEditor>("Game Data Editor", true);
        editor.db = Selection.activeObject as GameDataLibrary;

        if(editor.db == null)
        {
            editor.db = FindAndLoadDB<GameDataLibrary>();
        }

        if(editor.db == null)
        {
            editor.db = ScriptableObject.CreateInstance<GameDataLibrary>();
            editor.db.characters = new List<EntityStats>();
            editor.db.powerUps = new List<PowerUpDefinition>();
        }

        editor.SetInitialData(GameDataDBEditor.DBType.Characters, string.Empty);
    }

    public static T FindAndLoadDB<T> () where T : ScriptableObject
    {
        string assetTypeName = typeof(T).Name;
        string[] searchResults = AssetDatabase.FindAssets (string.Format ("t:{0}", assetTypeName));
        if (searchResults.Length > 0) {
            string assetPath = AssetDatabase.GUIDToAssetPath (searchResults [0]);
            var toReturn = AssetDatabase.LoadAssetAtPath<T> (assetPath);
            return toReturn;
        } else {
            Debug.LogErrorFormat ("Unable to find ScriptableObject of type: {0}, Returning empty instance", assetTypeName);
            return ScriptableObject.CreateInstance<T> ();
        }
    }

    [UnityEditor.Callbacks.OnOpenAsset (1)]
    public static bool OnOpenAsset (int instanceID, int line)
    {
        if (Selection.activeObject as GameDataLibrary) 
        {
            ShowGameDataDBEditor ();
            return true;
        }
        return false;
    }
}