﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(PowerUpInstance))]
public class PowerUpInstanceEditor : Editor
{
    int selectedIndex;
    int currentIdIndex;

    GameDataLibrary lib = null;
    PowerUpDefinition def;
    PowerUpInstance instance;

    int powerUpPreviewId = -1;
    bool autoUpdatePreview;

    void OnEnable()
    {
        instance = this.target as PowerUpInstance;
        lib = GameDataDBMenuUtils.FindAndLoadDB<GameDataLibrary>();
        selectedIndex = !string.IsNullOrEmpty(instance.powerUpId) ? lib.powerUps.FindIndex(w=>w.Id.Equals(instance.powerUpId)) : 0;
        selectedIndex = Mathf.Max(selectedIndex,0);
        currentIdIndex = selectedIndex;
    }

    public override void OnInspectorGUI()
    {
        EditorGUILayout.LabelField(string.Format("PowerUp: {0}", instance.powerUpId));
        autoUpdatePreview = EditorGUILayout.Toggle("Auto Update Preview", autoUpdatePreview);

        if(lib.powerUps.Count == 0)
        {
            EditorGUILayout.HelpBox("Unable to find valid PowerUps DB. Please create one and add at least one PowerUp", MessageType.Error);
            return;
        }

        if(!string.IsNullOrEmpty(instance.powerUpId) && GUILayout.Button(string.Format("EDIT: [{0} - {1}]", lib.powerUps[currentIdIndex].Id, lib.powerUps[currentIdIndex].Name)))
        {
            GameDataDBMenuUtils.ShowPowerUpsDBEditor(instance.powerUpId);
        }

        DrawSelectorGUI();
        UpdatePowerUpGamePreview();
    }

    void DrawSelectorGUI()
    {
        def = lib.powerUps[selectedIndex];

        EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button("<<")) 
        {
            selectedIndex--;
            if (selectedIndex < 0) 
            {
                selectedIndex = 0;
            }
        }

        if (GUILayout.Button(string.Format("Set {0}: {1}-{2}",selectedIndex,def.Id,def.Name))) {
            Undo.RecordObject(instance, "CharacterEntityEditor-ChangeAttributes");

            instance.powerUpId = def.Id;
            currentIdIndex = selectedIndex;
        }

        if (GUILayout.Button(">>")) 
        {
            if (selectedIndex < lib.powerUps.Count - 1) {
                selectedIndex++;
            }
        }
        EditorGUILayout.EndHorizontal();
    }

    void UpdatePowerUpGamePreview()
    {
        if(!autoUpdatePreview)
        {
            return;
        }

        def = lib.powerUps[selectedIndex];

        if(powerUpPreviewId != selectedIndex && def != null && def.Prefab != null)
        {
            instance.powerUpDefinition = def;
            instance.InstantiateSelectedPowerUp();
            powerUpPreviewId = selectedIndex;
        }
    }
}

