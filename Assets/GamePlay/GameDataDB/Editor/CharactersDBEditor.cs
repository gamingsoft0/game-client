using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

/// <summary>
/// Editor window to manage all characters in the game
/// </summary>
public class CharactersDBEditor : EditorWindow
{
    public GameDataLibrary db;
    int selectedIndex;
    Vector2 scrollPos;

    /// <summary>
    /// Sets initial character to show by id
    /// </summary>
    /// <param name="id">Entity Id.</param>
    public void SetInitialData(string id)
    {
        selectedIndex = Mathf.Max(db.characters.FindIndex(w=>w.Id.Equals(id)),0);
    }

    public void OnGUI ()
    {
        DrawCharactersGUI (db.characters,EntityType.Character);

        if (GUI.changed) {
            EditorUtility.SetDirty (this.db);
        }
    }

    void DrawAddNewButton<T> (List<T> itemList, EntityType type) where T : EntityStats
    {
        if (itemList == null || itemList.Count == 0 && GUILayout.Button ("Add New " + type.ToString ())) {
            if (itemList == null) {
                itemList = new List<T> ();
            }
            var newItem = System.Activator.CreateInstance<T> ();
            itemList.Add (newItem);
            selectedIndex = itemList.Count - 1;
        }
    }

    void DrawCharactersList<T> (List<T> itemList, EntityType entityType) where T : EntityStats
    {
        if (itemList == null || itemList.Count == 0) { return; /* do nothing */}

        int currentIndex = selectedIndex;

        EditorGUILayout.BeginVertical ("box", GUILayout.MinWidth (100));

        for (int itemCount = 0, max = itemList.Count; itemCount < max; ++itemCount) {
            GUIStyle style = (itemCount == currentIndex) ? EditorStyles.toolbarButton : EditorStyles.miniButton;
            if (GUILayout.Button (string.Format ("{0} - {1}", itemCount.ToString ("000"), itemList [itemCount].Id), style)) {
                currentIndex = itemCount;
            }
        }

        selectedIndex = currentIndex;

        EditorGUILayout.EndVertical ();
    }

    void DrawListCursorButtons<T> (List<T> itemList, EntityType entityType) where T : EntityStats
    {
        if (itemList == null || itemList.Count == 0) { return; /* do nothing */}

        int currentIndex = selectedIndex;
        EditorGUILayout.BeginHorizontal ("box");

        if (GUILayout.Button ("<<")) {
            currentIndex = 0;
        } else if (GUILayout.Button ("<")) {
            currentIndex -= 1;
            if (currentIndex < 0) {
                currentIndex = itemList.Count - 1;
            }
        } else if (GUILayout.Button (">")) {
            currentIndex += 1;
            if (currentIndex >= itemList.Count) {
                currentIndex = 0;
            }
        } else if (GUILayout.Button (">>")) {
            currentIndex = itemList.Count - 1;
        } else if (GUILayout.Button ("+")) {
            T newItem = System.Activator.CreateInstance<T> ();
            itemList.Add (newItem);
            currentIndex = itemList.Count - 1;
        } else if (GUILayout.Button ("-")) {
            itemList.RemoveAt (currentIndex);
            if (currentIndex >= itemList.Count) {
                currentIndex = itemList.Count - 1;
            }
        }

        selectedIndex = currentIndex;

        EditorGUILayout.EndHorizontal ();
    }

    void DrawBaseCharacterGUI (EntityStats entityStat)
    {
        entityStat.Health = EditorGUILayout.IntSlider ("Base Health", entityStat.Health, 1, 999);
        entityStat.Strength = EditorGUILayout.IntSlider ("Base Strength", entityStat.Strength, 1, 999);
        entityStat.Defense = EditorGUILayout.IntSlider ("Base Defense", entityStat.Defense, 1, 999);
        entityStat.MagicDefense = EditorGUILayout.IntSlider ("Base Magic Defense", entityStat.MagicDefense, 1, 999);
        entityStat.Agility = EditorGUILayout.IntSlider ("Base Agility", entityStat.Agility, 1, 999);
        entityStat.Wisdom = EditorGUILayout.IntSlider ("Base Wisdom", entityStat.Wisdom, 1, 999);
        entityStat.HitChance = EditorGUILayout.IntSlider ("Base Hit Chance", entityStat.HitChance, 1, 999);
        entityStat.AttackCooldown = EditorGUILayout.Slider(new GUIContent("Attack Cooldown","Time to wait between each attack. Useful for enemies"),entityStat.AttackCooldown,0.1f,10f);
        entityStat.DetectionRange = EditorGUILayout.IntSlider(new GUIContent("Detection Range","Useful for enemies"), entityStat.DetectionRange, 1, 2000);
    }

    void DrawRandomSfxGUI(ActionType actionType, EntityStats entityStat)
    {
        List<AudioClip> clipsList = null;
        switch (actionType)
        {
            case ActionType.Attack:
                clipsList = entityStat.AttackAudio;
                break;
            case ActionType.Skill:
                clipsList = entityStat.SkillsAudio;
                break;
        }

        EditorGUILayout.BeginVertical ("box");
        EditorGUILayout.LabelField ( string.Format("{0} Audio Clips",actionType.ToString()),EditorStyles.boldLabel);
        if (clipsList == null) 
        {
            clipsList = new List<AudioClip> ();
        }
        EditorGUI.indentLevel++;
        if (clipsList.Count > 0) 
        {
            int idToBeDeleted = -1;
            for (int i = 0, max = clipsList.Count; i < max; ++i) 
            {
                EditorGUILayout.BeginHorizontal ();
                clipsList [i] = EditorGUILayout.ObjectField (string.Format ("Audio {0}",i), clipsList [i], typeof(AudioClip), false) as AudioClip;
                if (GUILayout.Button(">")) {
                    EditorSFX.PlayClip(clipsList [i]);
                }
                if (GUILayout.Button ("X")){
                    idToBeDeleted = i;
                }
                EditorGUILayout.EndHorizontal ();
            }

            if (idToBeDeleted >= 0) {
                clipsList.RemoveAt (idToBeDeleted);
            }
        }

        // add new ...
        if (GUILayout.Button("Add new item"))
        {
            clipsList.Add (null);
        }

        EditorGUI.indentLevel--;
        EditorGUILayout.EndVertical ();
    }

    void DrawMovementConfig(EntityStats entityStat)
    {
        EditorGUILayout.LabelField("Movement Config",EditorStyles.boldLabel);
        EditorGUI.indentLevel++;
        if(entityStat.MovementConfig == null)
        {
            entityStat.MovementConfig = new CharacterController.MovementConfig();
        }
        entityStat.MovementConfig.MaxSpeed = EditorGUILayout.Slider ("Max Speed", entityStat.MovementConfig.MaxSpeed, 1, 10);
        entityStat.MovementConfig.JumpPower = EditorGUILayout.Slider("Jump Power", entityStat.MovementConfig.JumpPower, 1, 12);
        entityStat.MovementConfig.GravityMultiplier = EditorGUILayout.Slider("Gravity Multiplied", entityStat.MovementConfig.GravityMultiplier, 1, 4);
        entityStat.MovementConfig.CombatTimeDuration = EditorGUILayout.IntSlider("Combat Time", entityStat.MovementConfig.CombatTimeDuration, 1, 10);
        EditorGUI.indentLevel--;
    }

    void DrawElementalResistance(EntityStats entityStat) {
        EditorGUILayout.BeginVertical ();

        EditorGUILayout.LabelField ("Element Resistance", EditorStyles.boldLabel);
        EditorGUI.indentLevel++;
        if (entityStat.ElementalResist != null && entityStat.ElementalResist.Count > 0) {
            int idToBeDeleted = -1;
            for (int index = 0, max = entityStat.ElementalResist.Count; index < max; ++index) {
                EditorGUILayout.BeginHorizontal ();
                entityStat.ElementalResist [index].element = (ElementType)EditorGUILayout.EnumPopup (entityStat.ElementalResist [index].element);
                entityStat.ElementalResist [index].resistance = EditorGUILayout.Slider (entityStat.ElementalResist [index].resistance, 0f, 1f);
                if (GUILayout.Button ("X")) {
                    idToBeDeleted = index;
                }
                EditorGUILayout.EndHorizontal ();
            }

            if (idToBeDeleted >= 0) {
                entityStat.ElementalResist.RemoveAt (idToBeDeleted);
            }
        }
        // add new ...
        if (GUILayout.Button("Add new Element"))
        {
            if (entityStat.ElementalResist == null)
            {
                entityStat.ElementalResist = new List<ElementalResistData>();
            }
            entityStat.ElementalResist.Add(new ElementalResistData());
        }

        EditorGUI.indentLevel--;
        EditorGUILayout.EndVertical();
    }

    void DrawCharactersGUI<T> (List<T> itemList, EntityType entityType) where T : EntityStats
    {
        DrawAddNewButton (itemList, entityType);

        if (itemList == null || itemList.Count == 0) { return; /* do nothing */}
        EntityStats entityStat = null;

        if (itemList != null && itemList.Count > 0) {
            EditorGUILayout.BeginHorizontal ();
            DrawCharactersList (itemList, entityType);
            EditorGUILayout.BeginVertical ();

            DrawListCursorButtons (itemList, entityType);

            if(selectedIndex < 0 || selectedIndex >= itemList.Count) { 
                // there is no details to show because items list is empty
                return; 
            }

            entityStat = itemList [selectedIndex];

            EditorGUILayout.BeginVertical ("box");
            if (entityStat != null) {
                EditorGUILayout.LabelField (string.Format ("Editing: {0} of {1}", +selectedIndex + 1, itemList.Count));
                DrawCharacterGUI (entityStat);
            }
            EditorGUILayout.EndVertical ();
        }
    }

    void DrawCharacterPrefab(EntityStats entityStat)
    {
        EditorGUILayout.BeginVertical("box");

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField ("Prefab",GUILayout.Width(50));
        entityStat.Prefab = EditorGUILayout.ObjectField(entityStat.Prefab, typeof(GameObject), false) as GameObject;
        EditorGUILayout.EndHorizontal();

        if (entityStat.Prefab != null) {
            Texture2D itemPrefabPreview = AssetPreview.GetAssetPreview(entityStat.Prefab);
            if (itemPrefabPreview != null && !AssetPreview.IsLoadingAssetPreview(entityStat.Prefab.GetInstanceID())){
                GUILayout.Box(itemPrefabPreview,GUILayout.Width(64),GUILayout.Height(64));
            }
        }
        EditorGUILayout.EndVertical();
    }

    void DrawCharacterGUI(EntityStats entityStat)
    {        
        scrollPos = EditorGUILayout.BeginScrollView (scrollPos);

        entityStat.Id = EditorGUILayout.TextField ("Character Id", entityStat.Id);
        entityStat.Name = EditorGUILayout.TextField ("Character Name", entityStat.Name);
        DrawBaseCharacterGUI (entityStat);

        EditorGUIUtility.fieldWidth = 100;

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.BeginVertical();
        DrawMovementConfig(entityStat);
        EditorGUILayout.EndVertical();
        EditorGUILayout.BeginVertical();
        DrawCharacterPrefab(entityStat);
        EditorGUILayout.EndVertical();
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal ();
        DrawRandomSfxGUI (ActionType.Attack, entityStat);
        DrawRandomSfxGUI (ActionType.Skill, entityStat);
        EditorGUILayout.EndHorizontal ();

        DrawElementalResistance (entityStat);

        EditorGUILayout.EndScrollView ();
    }
}