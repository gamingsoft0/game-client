﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(CharacterEntity))]
public class CharacterEntityEditor : Editor
{
    int characterIndex = 0;
    int currentCharacterIndex;
    GameDataLibrary lib = null;
    EntityStats templateEntity;

    CharacterEntity entity;

    void OnEnable() 
    {
        entity = target as CharacterEntity;
        lib = GameDataDBMenuUtils.FindAndLoadDB<GameDataLibrary> ();
        characterIndex = !string.IsNullOrEmpty (entity.CharacterId) ? lib.characters.FindIndex (w => w.Id.Equals (entity.CharacterId)) : 0;
        currentCharacterIndex = characterIndex;
    }

    public override void OnInspectorGUI () 
    {
        entity.Type = (EntityType)EditorGUILayout.EnumPopup("Type", entity.Type);
        entity.IsBoss = EditorGUILayout.Toggle ("Is Boss", entity.IsBoss);
        entity.EntityCollider = (CapsuleCollider)EditorGUILayout.ObjectField ("Entity Collider", entity.EntityCollider, typeof(CapsuleCollider), true);
        entity.MovementComponent = (CharacterController)EditorGUILayout.ObjectField ("Movement Component", entity.MovementComponent, typeof(CharacterController), true);

        if (lib.characters.Count == 0)
        {
            EditorGUILayout.HelpBox ("Unable to find valid Characters DB. Please create one and add at least one character", MessageType.Error);
            return;
        }

        if (currentCharacterIndex >= 0 && GUILayout.Button(string.Format("EDIT: [{0} - {1}]", lib.characters[currentCharacterIndex].Id, lib.characters[currentCharacterIndex].Name)))
        {
            GameDataDBMenuUtils.ShowCharactersDBEditor(entity.CharacterId);
        }

        templateEntity = lib.characters[characterIndex];

        EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button("<<")) 
        {
            characterIndex--;
            if (characterIndex < 0) 
            {
                characterIndex = 0;
            }
        }
        if (GUILayout.Button(string.Format("Set {0}: {1}-{2}",characterIndex,templateEntity.Id,templateEntity.Name))) {
            Undo.RecordObject(entity, "CharacterEntityEditor-ChangeAttributes");

            entity.CharacterId = templateEntity.Id;
            currentCharacterIndex = characterIndex;

            if (entity.InteracterDM == null) {
                entity.InteracterDM = new InteracterDM (templateEntity);
            } else {
                entity.InteracterDM.AssignStats(templateEntity);
            }

            entity.InstantiateSelectedPrefab(); // replace prefab
        }
        if (GUILayout.Button(">>")) 
        {
            if (characterIndex < lib.characters.Count - 1) {
                characterIndex++;
            }
        }
        EditorGUILayout.EndHorizontal();
    }
}
