﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

/// <summary>
/// Power ups DB editor. Used to manage all PowerUps in the game
/// </summary>
public class PowerUpsDBEditor : EditorWindow
{
    public GameDataLibrary db;
    int selectedIndex;

    /// <summary>
    /// Sets initial powerup to show by id
    /// </summary>
    /// <param name="powerUpId">Power up identifier.</param>
    public void SetInitialData(string powerUpId)
    {
        selectedIndex = Mathf.Max(db.powerUps.FindIndex(w => w.Id == powerUpId),0);
    }

    public void OnGUI()
    {
        DrawPowerUpsGUI(db.powerUps);
        if(GUI.changed)
        {
            EditorUtility.SetDirty(this.db);
        }
    }

    void DrawAddNewButton<T> (List<T> itemList) where T : PowerUpDefinition
    {
        if (itemList == null || itemList.Count == 0 && GUILayout.Button ("Add New")) 
        {
            if (itemList == null) 
            {
                itemList = new List<T> ();
            }
            var newItem = System.Activator.CreateInstance<T> ();
            itemList.Add (newItem);
            selectedIndex = itemList.Count - 1;
        }
    }

    void DrawPowerUpsList<T> (List<T> itemList) where T : PowerUpDefinition
    {
        if (itemList == null || itemList.Count == 0) { return; /* do nothing */}

        int currentIndex = selectedIndex;

        EditorGUILayout.BeginVertical ("box", GUILayout.MinWidth (100));

        for (int itemCount = 0, max = itemList.Count; itemCount < max; ++itemCount) {
            GUIStyle style = (itemCount == currentIndex) ? EditorStyles.toolbarButton : EditorStyles.miniButton;
            if (GUILayout.Button (string.Format ("{0} - {1}", itemCount.ToString ("000"), itemList [itemCount].Id), style)) {
                currentIndex = itemCount;
            }
        }

        selectedIndex = currentIndex;

        EditorGUILayout.EndVertical ();
    }

    void DrawListCursorButtons<T> (List<T> itemList) where T : PowerUpDefinition
    {
        if (itemList == null || itemList.Count == 0) { return; /* do nothing */}

        int currentIndex = selectedIndex;
        EditorGUILayout.BeginHorizontal ("box");

        if (GUILayout.Button ("<<")) {
            currentIndex = 0;
        } else if (GUILayout.Button ("<")) {
            currentIndex -= 1;
            if (currentIndex < 0) {
                currentIndex = itemList.Count - 1;
            }
        } else if (GUILayout.Button (">")) {
            currentIndex += 1;
            if (currentIndex >= itemList.Count) {
                currentIndex = 0;
            }
        } else if (GUILayout.Button (">>")) {
            currentIndex = itemList.Count - 1;
        } else if (GUILayout.Button ("+")) {
            T newItem = System.Activator.CreateInstance<T> ();
            itemList.Add (newItem);
            currentIndex = itemList.Count - 1;
        } else if (GUILayout.Button ("-")) {
            itemList.RemoveAt (currentIndex);
            if (currentIndex >= itemList.Count) {
                currentIndex = itemList.Count - 1;
            }
        }

        selectedIndex = currentIndex;

        EditorGUILayout.EndHorizontal ();
    }

    void DrawPowerUpsGUI<T> (List<T> itemList) where T : PowerUpDefinition
    {
        DrawAddNewButton (itemList);

        if (itemList == null || itemList.Count == 0) { return; /* do nothing */}
        PowerUpDefinition def = null;

        if (itemList != null && itemList.Count > 0) {
            EditorGUILayout.BeginHorizontal ();
            DrawPowerUpsList (itemList);
            EditorGUILayout.BeginVertical ();

            DrawListCursorButtons (itemList);

            if(selectedIndex < 0 || selectedIndex >= itemList.Count) { 
                // there is no details to show because items list is empty
                return; 
            }

            def = itemList [selectedIndex];

            EditorGUILayout.BeginVertical ("box");
            if (def != null) {
                EditorGUILayout.LabelField (string.Format ("Editing: {0} of {1}", +selectedIndex + 1, itemList.Count));
                DrawPowerUpGUI (def);
            }
            EditorGUILayout.EndVertical ();
        }
    }

    void DrawPowerUpGUI(PowerUpDefinition def)
    {
        def.Id = EditorGUILayout.TextField ("Id", def.Id);
        def.Name = EditorGUILayout.TextField ("Name", def.Name);
        def.Type = (PowerUpType)EditorGUILayout.EnumPopup ("Type", def.Type);
        def.Amount = EditorGUILayout.IntSlider ("Amount", def.Amount, 1, 999);
        def.TimeSpan = EditorGUILayout.DelayedFloatField ("TimeSpan", def.TimeSpan);
        def.PickUpSound = EditorGUILayout.ObjectField("PickUp Sfx", def.PickUpSound, typeof(AudioClip), false) as AudioClip;

        EditorGUIUtility.fieldWidth = 100;

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField ("Prefab", GUILayout.Width(150));
        def.Prefab = EditorGUILayout.ObjectField(def.Prefab, typeof(GameObject), false) as GameObject;
        if (def.Prefab != null) {
            Texture2D itemPrefabPreview = AssetPreview.GetAssetPreview(def.Prefab);
            if (itemPrefabPreview != null && !AssetPreview.IsLoadingAssetPreview(def.Prefab.GetInstanceID())){
                GUILayout.Box(itemPrefabPreview,GUILayout.Width(64),GUILayout.Height(64));
            }
        }
        EditorGUILayout.EndHorizontal();
    }
}
