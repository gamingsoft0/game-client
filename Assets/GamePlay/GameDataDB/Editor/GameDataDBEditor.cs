﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class GameDataDBEditor : EditorWindow {

    public enum DBType
    {
        Characters, PowerUps
    }

    public GameDataLibrary db;
    Dictionary<DBType, int> selectedIndex;
    int selectedTab;

    PowerUpsDBEditor m_PowerUpsEditor;
    CharactersDBEditor m_CharactersEditor;

    public void SetInitialData(DBType dbType, string elementId)
    {
        selectedTab = (int)dbType;

        m_PowerUpsEditor = ScriptableObject.CreateInstance<PowerUpsDBEditor>();
        m_PowerUpsEditor.db = db;
        m_CharactersEditor = ScriptableObject.CreateInstance<CharactersDBEditor>();
        m_CharactersEditor.db = db;

        switch(dbType)
        {
            case DBType.Characters:
                m_CharactersEditor.SetInitialData(elementId);
                break;
            case DBType.PowerUps:
                m_PowerUpsEditor.SetInitialData(elementId);
                break;
        }
    }

    void OnGUI()
    {
        string[] tabs = System.Enum.GetNames(typeof(DBType));
        selectedTab = GUILayout.Toolbar(selectedTab, tabs);

        switch(selectedTab)
        {
            case (int)DBType.Characters:
                m_CharactersEditor.OnGUI();
                break;
            case (int)DBType.PowerUps:
                m_PowerUpsEditor.OnGUI();
                break;
        }

        if(GUI.changed)
        {
            EditorUtility.SetDirty(db);
        }
    }
}
