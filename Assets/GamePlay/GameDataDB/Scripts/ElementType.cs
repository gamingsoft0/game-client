﻿public enum ElementType {
    None = 0,
    Fire = 1,
    Ice = 2,
    Earth = 3,
    Lightning = 4,
    Holy = 5,
    Dark = 6
}