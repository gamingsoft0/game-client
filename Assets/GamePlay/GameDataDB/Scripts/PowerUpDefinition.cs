﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PowerUpDefinition  
{
    /// <summary>
    /// PowerUp identifier.
    /// </summary>
    public string Id;
    /// <summary>
    /// Name of the PowerUp
    /// </summary>
    public string Name;
    /// <summary>
    /// Type of power up
    /// </summary>
    public PowerUpType Type;
    /// <summary>
    /// Value related to power up effect
    /// </summary>
    public int Amount;
    /// <summary>
    /// How long is this power up active?
    /// </summary>
    public float TimeSpan;
    /// <summary>
    /// Pick up sound.
    /// </summary>
    public AudioClip PickUpSound;
    /// <summary>
    /// Related Prefab
    /// </summary>
    public GameObject Prefab;
}
