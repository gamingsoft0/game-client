using System.Collections.Generic;
using UnityEngine;

[Prefab]
public class GameDataDB : Singleton<GameDataDB>
{
    [SerializeField] GameDataLibrary m_GameDataLibrary;

    public EntityStats GetCharacterDefinitionById (string characterId)
    {
        return m_GameDataLibrary.GetCharacterById (characterId);
    }

    public PowerUpDefinition GetPowerUpDefinitionById(string id)
    {
        return m_GameDataLibrary.GetPowerUpDefinitionById(id);
    }

    public override void OnDestroy()
    {
        m_Instance = null; // singleton can be safely removed
    }
}