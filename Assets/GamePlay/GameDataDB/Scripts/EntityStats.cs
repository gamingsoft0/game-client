﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ElementalResistData
{    
    public ElementType element;
    [Range(0f,1f)] public float resistance;
};

public enum ActionType 
{
    Attack,
    Jump,
    Skill
}

[System.Serializable]
public class EntityStats
{
    public string Id;
    public string Name;
    public GameObject Prefab;

    public int Health;
    public int Agility;
    public int Strength;
    public int Defense;
    public int MagicDefense;
    public int Wisdom;
    public int HitChance;

    public float AttackCooldown;
    public int DetectionRange;

    public CharacterController.MovementConfig MovementConfig;

    public List<AudioClip> AttackAudio;
    public List<AudioClip> SkillsAudio;

    public List<ElementalResistData> ElementalResist;
}