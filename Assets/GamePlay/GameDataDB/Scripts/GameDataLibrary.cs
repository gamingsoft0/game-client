﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameDataDB", menuName = "Assets/Game Data/Create GameData DB")]
public class GameDataLibrary : ScriptableObject, ISerializationCallbackReceiver
{
    public List<EntityStats> characters;
    public Dictionary<string,EntityStats> entities;

    public List<PowerUpDefinition> powerUps;
    public Dictionary<string,PowerUpDefinition> powerUpsDict;

    public EntityStats GetCharacterById(string id)
    {
        return entities.TryGetValue (id);
    }

    public PowerUpDefinition GetPowerUpDefinitionById(string id)
    {
        return powerUpsDict.TryGetValue(id);
    }

    public void OnAfterDeserialize()
    {
        this.entities = new Dictionary<string, EntityStats> ();

        foreach (var item in characters)
        {
            this.entities[item.Id] = item;
        }

        powerUpsDict = new Dictionary<string, PowerUpDefinition>();

        if(powerUps == null) { powerUps = new List<PowerUpDefinition>(); }

        foreach(var item in powerUps)
        {
            this.powerUpsDict[item.Id] = item;
        }
    }

    public void OnBeforeSerialize()
    {
        return; // do nothing ...
    }
}
