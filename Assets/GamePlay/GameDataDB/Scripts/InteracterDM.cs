﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class InteracterDM : EntityStats
{
    public float CurrentHealthPoints;
    public float CurrentMagicPoints;

    public void AddLifePoints(float healthPoints)
    {
        this.CurrentHealthPoints = Mathf.Max(Mathf.Min (this.CurrentHealthPoints + healthPoints, this.Health),0);
    }

    public void AddMagicPoints(float magicPoints)
    {
        this.CurrentMagicPoints = Mathf.Max(Mathf.Min (this.CurrentMagicPoints + magicPoints, this.Wisdom),0);
    }

    public InteracterDM(EntityStats stats)
    {
        CurrentHealthPoints = stats.Health;
        CurrentMagicPoints = stats.Wisdom;
        AssignStats (stats);
    }

    public void AssignStats (EntityStats stats)
    {
        this.Id         = stats.Id;
        this.Name       = stats.Name;
        this.Prefab     = stats.Prefab;
        this.Health     = stats.Health;
        this.Agility    = stats.Agility;
        this.Strength   = stats.Strength;
        this.Defense    = stats.Defense;
        this.MagicDefense = stats.MagicDefense;
        this.Wisdom     = stats.Wisdom;
        this.HitChance  = stats.HitChance;
        this.MovementConfig = stats.MovementConfig;
        this.DetectionRange = stats.DetectionRange;
        this.AttackAudio = stats.AttackAudio;
        this.SkillsAudio = stats.SkillsAudio;
    }
}

