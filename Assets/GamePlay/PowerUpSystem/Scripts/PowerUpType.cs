﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PowerUpType
{
    HealthPack,
    MagicPack,
    Coin,
    Spell01
}
