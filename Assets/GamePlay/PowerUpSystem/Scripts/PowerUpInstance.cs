﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpInstance : MonoBehaviour {

    public string powerUpId;
    public string powerUpName;
    [System.NonSerialized] public PowerUpDefinition powerUpDefinition;
    private GameObject powerUpObject;

    void Start()
    {
        powerUpDefinition = GameDataDB.Instance.GetPowerUpDefinitionById(powerUpId);
        powerUpName = powerUpDefinition.Name;
        InstantiateSelectedPowerUp();
    }

    public void InstantiateSelectedPowerUp()
    {
        if(powerUpDefinition == null)
        {
            Debug.LogErrorFormat("Unable to instantiate PowerUpPrefab. invalid definition for id {0}",powerUpId);
            return;
        }

        transform.DestroyChildren(); // clean up children
        powerUpObject = Object.Instantiate(powerUpDefinition.Prefab, Vector3.zero, Quaternion.identity);
        powerUpObject.transform.SetParent(this.transform, false);
        powerUpObject.AddComponent<InteractablePowerUp>();
        Rigidbody powerUpRigidBody = powerUpObject.AddComponent<Rigidbody>();
        powerUpRigidBody.isKinematic = true;
    }
}
