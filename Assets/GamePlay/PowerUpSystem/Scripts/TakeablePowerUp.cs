﻿using UnityEngine;
using System.Collections;

public class TakeablePowerUp : MonoBehaviour
{
    public const string PLAYER_TAG = "Player";

    CustomizablePowerUp customizablePowerUp;

    protected virtual void Start()
    {
        customizablePowerUp = transform.GetComponentInParent<CustomizablePowerUp>();
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.tag == PLAYER_TAG)
        {
            if(!HandleInteract(other))
            {
                return;
            }

            Destroy(transform.parent.gameObject);
        }
    }

    protected virtual bool HandleInteract(Collider other)
    {
        if(customizablePowerUp.pickUpSound != null)
        {
            AudioSource.PlayClipAtPoint(customizablePowerUp.pickUpSound, transform.position);
        }

        return true;
    }
}
