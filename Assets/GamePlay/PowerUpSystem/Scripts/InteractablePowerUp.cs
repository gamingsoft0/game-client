﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractablePowerUp : TakeablePowerUp {

    PowerUpInstance customPowerUp;
    CharacterEntity entity;

    protected override void Start()
    {
        customPowerUp = transform.GetComponentInParent<PowerUpInstance>();
        if(customPowerUp == null)
        {
            Debug.LogErrorFormat("Please add a {0} component to this object. Destroying {1}",typeof(PowerUpInstance).Name,typeof(TakeablePowerUp).Name);
            Object.Destroy(this);
        }
    }

    protected override bool HandleInteract(Collider other)
    {
        CharacterEntity character = other.GetComponentInParent<CharacterEntity> ();
        if(character != null && character != entity)
        {
            GameManager.Instance.HandlePowerUp(customPowerUp.powerUpDefinition,character);

            if(customPowerUp.powerUpDefinition.PickUpSound != null)
            {
                AudioManager.Instance.PlayClip (customPowerUp.powerUpDefinition.PickUpSound);
            }
            entity = character;
            return true;
        }
        return false;
    }
}
