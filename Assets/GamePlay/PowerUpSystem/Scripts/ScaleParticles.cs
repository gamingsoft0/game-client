﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class ScaleParticles : MonoBehaviour {
	void Update () 
    {
        var mainParticleSystem = GetComponent<ParticleSystem>().main;
        mainParticleSystem.startSize = transform.lossyScale.magnitude;
	}
}