﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class NewPowerUpCollectedPopUpController : MonoBehaviour 
{
    const float RESUME_TIME_SECONDS = 0.5f;

    [SerializeField] UIElementBase m_viewContainer;
    [SerializeField] Text m_NewPowerUpTitle;
    [SerializeField] Text m_NewPowerUpDesc;
    [SerializeField] Image m_IconContainer;

    [Header("New Power Up Collected Messages")]
    [SerializeField] [TextArea] string m_CoinsPowerUpMessage;
    [SerializeField] [TextArea] string m_HealPowerUpMessage;
    [SerializeField] [TextArea] string m_MagicPowerUpMessage;
    [SerializeField] [TextArea] string m_SpellPowerUpMessage;

    [Header("New Power Up Icons")]
    [SerializeField] Sprite m_CoinsPowerUpIcon;
    [SerializeField] Sprite m_HealPowerUpIcon;
    [SerializeField] Sprite m_MagicPowerUpIcon;
    [SerializeField] Sprite m_SpellPowerUpIcon;

    public System.Action OnHide = delegate {};    

    public void Show(PowerUpDefinition def)
    {
        GameManager.Instance.StartCoroutine (DisableTimeCoroutine ());

        m_NewPowerUpTitle.text = def.Name;

        switch (def.Type) 
        {
        case PowerUpType.Coin:
            m_NewPowerUpDesc.text = m_CoinsPowerUpMessage;
            m_IconContainer.sprite = m_CoinsPowerUpIcon;
                break;
        case PowerUpType.HealthPack:
            m_NewPowerUpDesc.text = m_HealPowerUpMessage;
            m_IconContainer.sprite = m_HealPowerUpIcon;
            break;
        case PowerUpType.MagicPack:
            m_NewPowerUpDesc.text = m_MagicPowerUpMessage;
            m_IconContainer.sprite = m_MagicPowerUpIcon;
            break;
        case PowerUpType.Spell01:
            m_IconContainer.sprite = m_SpellPowerUpIcon;
            m_NewPowerUpDesc.text = m_SpellPowerUpMessage;
            break;
        }

        m_viewContainer.Show();
    }

    public void Hide ()
    {
        m_viewContainer.Hide();
        GameManager.Instance.StartCoroutine (ResumeTimeCoroutine ());
        OnHide ();
    }

    public IEnumerator DisableTimeCoroutine()
    {
        Time.timeScale = 1;

        var tweener = DOTween.To(()=>Time.timeScale, x=> Time.timeScale = x, 0, RESUME_TIME_SECONDS).SetEase(Ease.InQuad).SetUpdate(true);
        yield return tweener.WaitForCompletion ();

        Time.timeScale = 0;
    }

    public IEnumerator ResumeTimeCoroutine()
    {
        Time.timeScale = 0;

        var tweener = DOTween.To(()=>Time.timeScale, x=> Time.timeScale = x, 1, RESUME_TIME_SECONDS).SetEase(Ease.InQuad).SetUpdate(true);
        yield return tweener.WaitForCompletion ();

        Time.timeScale = 1;
    }
}
