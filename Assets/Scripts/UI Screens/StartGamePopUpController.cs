﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartGamePopUpController : MonoBehaviour 
{
    [SerializeField] UIElementBase m_viewContainer;
    
    private void Start()
    {   // show view by default
        m_viewContainer.Show();
        Time.timeScale = 0;
    }

    public void Close()
    {
        m_viewContainer.Hide();
        Time.timeScale = 1;
    }
}
