﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class EndGamePopUpController : MonoBehaviour 
{
    [System.Serializable]
    public struct StatsBar
    {
        public Slider slider;
        public Text valueText;

        public void ShowAnimated(int value, int max, float time = 1f)
        {
            slider.minValue = 0; slider.maxValue = max;
            slider.value = 0;
            valueText.text = string.Format("{0}/{1}",0,max);
            GameManager.Instance.StartCoroutine(ShowAnimatedCoroutine(value,max,time));
        }

        IEnumerator ShowAnimatedCoroutine(int value, int max, float time)
        {
            slider.DOValue(value,time);
            float endTime = Time.time + time;
            float valueToShow = 0;

            DOTween.To(() => valueToShow, x=> valueToShow = x, value, time); 
            while(Time.time < endTime)
            {
                valueText.text = string.Format("{0}/{1}",(int)valueToShow,max);
                yield return null;
            }
            valueText.text = string.Format("{0}/{1}",value,max);
        }
    }

    [SerializeField] UIElementBase m_viewContainer;
    [SerializeField] GameObject m_YouWinGo;
    [SerializeField] GameObject m_YouLoseGo;
    [SerializeField] StatsBar m_CoinsCollectedStat;
    [SerializeField] StatsBar m_HealthPacksUsed;
    [SerializeField] StatsBar m_EnemiesDefeatedStat;
    [SerializeField] Text m_time_taken;    

    public void Show(bool youWin)
    {
        GameProgress.Instance.EndGameTimer();

        m_viewContainer.Show();

        m_YouWinGo.SetActive(youWin);
        m_YouLoseGo.SetActive(!youWin);

        var powerUpsInfo = GameProgress.Instance.CurrentGameStats.collectedPowerUps;
        var coinsInfo = powerUpsInfo.Find(w => w.Type == PowerUpType.Coin);
        var healthInfo = powerUpsInfo.Find(w => w.Type == PowerUpType.HealthPack);

        m_CoinsCollectedStat.ShowAnimated(coinsInfo.Amount, coinsInfo.Max, 1f);
        m_HealthPacksUsed.ShowAnimated(healthInfo.Amount, healthInfo.Max, 1.2f);
        m_EnemiesDefeatedStat.ShowAnimated(GameProgress.Instance.CurrentGameStats.EnemiesDefeated, GameProgress.Instance.CurrentGameStats.EnemiesInMap, 1.5f);
        m_time_taken.text = GameProgress.Instance.CurrentGameStats.TotalTimeString;

    }

    public void HandleGameRestart()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex);
    }
}
