﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class GameManager : Singleton<GameManager> 
{    
    [SerializeField] Cinemachine.CinemachineTargetGroup m_VirtualCamera;

    CharacterEntity m_MainPlayerEntity;

    [SerializeField] Image m_MainAvarImage;
    [SerializeField] HpDisplayBar m_MainHpDisplayBar;
    [SerializeField] MpDisplayBar m_MainMpDisplayBar;
    [SerializeField] Text coinsText;
    [SerializeField] BulletShoot m_MeleeAttackPrefab;
    [SerializeField] BulletShoot[] m_MagicSpellPrefabs;
    [SerializeField] GameObject m_WorldContainer;
    [SerializeField] NewPowerUpCollectedPopUpController m_NewPowerUpPopUp;
    [SerializeField] EndGamePopUpController m_EndGamePopUp;
    [SerializeField] CanvasGroup m_MobileUI;

    public bool IsGameEnded { get; private set; }

    void Start()
    {
        GameProgress.Instance.StartGameTimer();

        GameProgress.Instance.OnPowerUpStatUpdated += GameProgress_Instance_OnPowerUpStatUpdated;
        UpdateMaxPowerUpValues();
        UpdateEnemiesInMap();

        m_NewPowerUpPopUp.OnHide = delegate { SetMobileUIState(true); };
    }

    void SetMobileUIState(bool state)
    {
        m_MobileUI.interactable = state; 
        m_MobileUI.blocksRaycasts = state;
        m_MobileUI.alpha = state ? 1 : 0;
    }

    void UpdateMaxPowerUpValues()
    {
        // get all powerUps
        var allPowerUps = m_WorldContainer.GetComponentsInChildren<PowerUpInstance>(true);
        Dictionary<PowerUpType,int> counter = new Dictionary<PowerUpType, int>();
        PowerUpDefinition def;
        foreach(PowerUpInstance item in allPowerUps)
        {
            def = GameDataDB.Instance.GetPowerUpDefinitionById(item.powerUpId);

            if(!counter.ContainsKey(def.Type))
            {
                counter[def.Type] = 1;
            }
            else
            {
                counter[def.Type]++;
            }
        }

        foreach(var item in counter)
        {
            GameProgress.Instance.CurrentGameStats.SetMaxValueForPowerUp(item.Key, item.Value);
        }
    }

    void UpdateEnemiesInMap()
    {
        var allCharacters = m_WorldContainer.GetComponentsInChildren<CharacterEntity>(true);
        int allEnemies = 0;
        foreach(CharacterEntity item in allCharacters)
        {
            if(item.Type == EntityType.Enemy)
            {
                allEnemies++;
            }
        }

        GameProgress.Instance.CurrentGameStats.EnemiesInMap = allEnemies;
    }

    public void ConfigMainPlayer(CharacterEntity mainPlayer)
    {
        m_MainPlayerEntity = mainPlayer;
        m_MainHpDisplayBar.Entity = m_MainPlayerEntity;
        m_MainMpDisplayBar.Entity = m_MainPlayerEntity;
        ConfigMainCameraTarget(m_MainPlayerEntity.transform);
        m_MainPlayerEntity.onHpChange += CheckForDeadPlayer;
    }

    /// <summary>
    /// Check if main player dies
    /// </summary>
    void CheckForDeadPlayer (CharacterEntity entity, float currentValue, float maxValue)
    {
        if(!entity.IsDead) { return; }
        m_MainAvarImage.DOColor(Color.grey, 1f);
        entity.onHpChange -= CheckForDeadPlayer;
        ShowEndGameScreen();
    }

    public void ConfigMainCameraTarget(Transform target)
    {
        m_VirtualCamera.m_Targets = new []{ new Cinemachine.CinemachineTargetGroup.Target{weight = 2, target = target} };
    }

    public void AddCameraTarget(Transform target, float weight)
    {
        List<Cinemachine.CinemachineTargetGroup.Target> targets = new List<Cinemachine.CinemachineTargetGroup.Target> (m_VirtualCamera.m_Targets);
        int targetIndex = targets.FindIndex (w => w.target == target);
        if (targetIndex >= 0) { // remove item and add it again (unable to modify struct
            targets.RemoveAt(targetIndex);
        } 
        targets.Add (new Cinemachine.CinemachineTargetGroup.Target{ target = target, weight = weight });
        m_VirtualCamera.m_Targets = targets.ToArray ();
    }

    public void RemoveCameraTarget(Transform target)
    {
        List<Cinemachine.CinemachineTargetGroup.Target> targets = new List<Cinemachine.CinemachineTargetGroup.Target> (m_VirtualCamera.m_Targets);
        int targetIndex = targets.FindIndex (w => w.target == target);
        if (targetIndex >= 0) 
        {
            targets.RemoveAt (targetIndex);
            m_VirtualCamera.m_Targets = targets.ToArray ();
        }
    }

    void GameProgress_Instance_OnPowerUpStatUpdated(PowerUpType type, GameProgress.PowerUpStat stat)
    {
        switch(type)
        {
            case PowerUpType.Coin:
                coinsText.text = stat.Amount.ToString();
                break;
        }
    }

    public void HandlePowerUp(PowerUpDefinition def, CharacterEntity entity)
    {
        if (GameProgress.Instance.AddCollectedPowerUp (def)) 
        {
            SetMobileUIState (false);
            m_NewPowerUpPopUp.Show (def);
        }

        switch(def.Type)
        {
            case PowerUpType.HealthPack:
                entity.ReceiveHeal(new AttackParams {  damage = def.Amount });
                break;
            case PowerUpType.MagicPack:
                entity.ReceiveMPHeal(new AttackParams{ damage = def.Amount });
                break;
        }
    }

    public BulletShoot GetBulletInstance()
    {
        BulletShoot newBullet = Object.Instantiate<BulletShoot>(m_MeleeAttackPrefab,Vector3.zero,Quaternion.identity);
        return newBullet;
    }

    public BulletShoot GetSpellInstance(int spellIndex)
    {
        BulletShoot newBullet = Object.Instantiate<BulletShoot> (m_MagicSpellPrefabs [spellIndex], Vector3.zero, Quaternion.identity);
        return newBullet;
    }

    public void ShowEndGameScreen()
    {
        SetMobileUIState (false);
        IsGameEnded = true;
        m_EndGamePopUp.Show(!m_MainPlayerEntity.IsDead);
    }

    public override void OnDestroy()
    {   
        m_MainPlayerEntity.onHpChange -= CheckForDeadPlayer;

        if(GameProgress.IsAwake)
        {
            GameProgress.Instance.OnPowerUpStatUpdated -= GameProgress_Instance_OnPowerUpStatUpdated;
        }

        m_Instance = null; // singleton that can be destroyed
    }
}
