﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Simple way of doing an audio manager that can handle many channels at different positions
/// </summary>
public class AudioManager : Singleton<AudioManager> {

    [Range(1,50)] [SerializeField] int maxChannelsToUse = 20;
    [SerializeField] UnityEngine.Audio.AudioMixerGroup sfxGroup = null;

    private Queue<AudioSource> audioSources = new Queue<AudioSource>();

    public void PlayClipAtLocation(AudioClip clip, Transform location, bool follow = false)
    {
        AudioSource source = GetFreeAudioSource ();

        if (source == null) 
        {
            Debug.LogWarningFormat ("Unable to play clip [{0}] because channels limit has been reached");
            return;
        }

        if (source.isPlaying) 
        {
            source.Stop ();
        }

        source.outputAudioMixerGroup = sfxGroup;
        source.clip = clip;
        source.spatialBlend = 0.5f; //  make sound partially 3D
        source.transform.position = location.position; // use target position as sound source
        source.PlayDelayed(0.1f);
        StartCoroutine (WaitForAudioSourceToBeFree (source, 0.1f));

        if (follow) 
        {
            FollowTransformUntilDone (source, transform, 0.1f);
        }
    }

    public void PlayClip(AudioClip clip)
    {
        AudioSource source = GetFreeAudioSource ();

        if (source == null) 
        {
            Debug.LogWarningFormat ("Unable to play clip [{0}] because channels limit has been reached");
            return;
        }

        if (source.isPlaying) {
            source.Stop ();
        }

        source.outputAudioMixerGroup = sfxGroup;
        source.clip = clip;
        source.spatialBlend = 0; // make sound full 2D
        source.PlayDelayed(0.1f);
        StartCoroutine (WaitForAudioSourceToBeFree (source, 0.1f));
    }

    public AudioSource GetFreeAudioSource()
    {
        if (audioSources != null && audioSources.Count > 0)
        {
            return audioSources.Dequeue ();
        }

        if (audioSources.Count == maxChannelsToUse) 
        {
            return null;
        }

        GameObject newAudioSourceGo = new GameObject ("AudioSource", typeof(AudioSource));
        newAudioSourceGo.transform.SetParent (this.transform, false);
        return newAudioSourceGo.GetComponent<AudioSource> ();
    }

    IEnumerator FollowTransformUntilDone(AudioSource source, Transform transform, float delay)
    {
        float endTime = Time.time +delay;
        while (Time.time < endTime) 
        {
            source.transform.position = transform.position;
            yield return null;
        }

        while (source.isPlaying && transform != null) 
        {
            source.transform.position = transform.position;
            yield return null;
        }
    }

    IEnumerator WaitForAudioSourceToBeFree(AudioSource source, float initialDelay)
    {
        yield return new WaitForSeconds (initialDelay);
        yield return  null;
        yield return new WaitWhile (() => source.isPlaying);
        source.clip = null;
        audioSources.Enqueue (source);
    }

    public override void OnDestroy ()
    {
        m_Instance = null; // this singleton can be destroyed safely
    }
}
