﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoMove : MonoBehaviour
{
    [SerializeField] Vector3 direction;
    [SerializeField] float speed;

    private Transform _transform;

    // Use this for initialization
	void Start () {
        _transform = GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update () {
        _transform.localPosition += direction * speed * Time.deltaTime;
	}
}
