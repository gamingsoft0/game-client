﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

/// <summary>
/// Used to make sure the GO is on the NavMesh.
/// https://forum.unity.com/threads/failed-to-create-agent-because-it-is-not-close-enough-to-the-navmesh.125593/
/// </summary>
[RequireComponent(typeof(NavMeshAgent))]
public class NavMeshInstantiator : MonoBehaviour
{
    [ContextMenu("Execute")]
    void OnEnable()
    {
        var agent = GetComponent<NavMeshAgent>();
        
        NavMeshHit closestHit;

        if (NavMesh.SamplePosition(gameObject.transform.position, out closestHit, 500f, NavMesh.AllAreas))
        {   // reposition game object using new position
            gameObject.transform.position = closestHit.position + Vector3.up * agent.baseOffset;
        }
        else
        {   // log error and disable agent
            agent.enabled = false;
            Debug.LogError("Could not find position on NavMesh!");
        }
    }
}
