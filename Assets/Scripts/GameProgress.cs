﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameProgress : Singleton<GameProgress> 
{
    const string PP_GAME_STATS = "game_stats";

    [System.Serializable]
    public class PowerUpStat
    {
        public PowerUpType Type;
        public int Amount;
        public int Max;
    }

    [System.Serializable]
    public class GameStats
    {
        public float InitTime, EndTime;
        public List<PowerUpStat> collectedPowerUps;
        public int EnemiesDefeated;
        public int EnemiesInMap;

        public float TotalTime{ get{ return EndTime - InitTime; }}
        public string TotalTimeString
        {
            get
            {   
                System.TimeSpan timeSpan = System.TimeSpan.FromSeconds ((double)TotalTime);
                return string.Format ("{0} minutes and {1} seconds",timeSpan.Hours * 60 + timeSpan.Minutes,timeSpan.Seconds);
            }
        }

        public string GetElepasedTimeString(float currentTime)
        {
            System.TimeSpan timeSpan = System.TimeSpan.FromSeconds (currentTime - InitTime);
            return string.Format ("{0}:{1}",timeSpan.Hours * 60 + timeSpan.Minutes,timeSpan.Seconds);
        }

        public Dictionary<PowerUpType,PowerUpStat> GetPowerUpsCollectedDictionary()
        {
            Dictionary<PowerUpType,PowerUpStat> toReturn = new Dictionary<PowerUpType, PowerUpStat>();

            if(collectedPowerUps == null || collectedPowerUps.Count == 0)
            {
                return toReturn;
            }

            foreach(var item in collectedPowerUps)
            {
                toReturn[item.Type] = item;
            }

            return toReturn;
        }

        public void SetPowerUpsCollectedList(Dictionary<PowerUpType,PowerUpStat> powerUpsCollectedDict)
        {
            if(collectedPowerUps == null) { collectedPowerUps = new List<PowerUpStat>(); }

            if(powerUpsCollectedDict == null || powerUpsCollectedDict.Count == 0)
            {
                return;
            }

            foreach(var item in powerUpsCollectedDict)
            {
                var listIndex = collectedPowerUps.FindIndex(w => w.Type == item.Key);
                if(listIndex >= 0)
                {
                    collectedPowerUps[listIndex].Amount = item.Value.Amount;
                }
                else
                {
                    collectedPowerUps.Add(item.Value);
                }
            }
        }

        public void SetMaxValueForPowerUp(PowerUpType type, int value)
        {
            if(collectedPowerUps == null)
            {
                collectedPowerUps = new List<PowerUpStat>();
            }

            int powerUpIndex = collectedPowerUps.FindIndex(w => w.Type == type);
            if(powerUpIndex >= 0)
            {
                collectedPowerUps[powerUpIndex].Max = value;
            }
            else
            {
                collectedPowerUps.Add(new PowerUpStat{ Type = type, Max = value });
            }
        }
    }

    public GameStats CurrentGameStats { get; private set; }
    Dictionary<PowerUpType,PowerUpStat> collectedPowerUps;

    public event System.Action<PowerUpType,PowerUpStat> OnPowerUpStatUpdated = delegate {};

    public void StartGameTimer(){
        if(CurrentGameStats == null)
        {
            CurrentGameStats = new GameStats ();
        }

        collectedPowerUps = new Dictionary<PowerUpType, PowerUpStat>();
        CurrentGameStats.InitTime = Time.timeSinceLevelLoad;
    }

    public void EndGameTimer(){
        CurrentGameStats.EndTime = Time.timeSinceLevelLoad;
        CurrentGameStats.SetPowerUpsCollectedList(collectedPowerUps);
    }

    public GameStats GetElapsedTime(){
        return CurrentGameStats;
    }

    /// <summary>
    /// Adds the collected power up.
    /// </summary>
    /// <returns><c>true</c>, if collected power up is the first one, <c>false</c> otherwise.</returns>
    /// <param name="def">Def.</param>
    public bool AddCollectedPowerUp(PowerUpDefinition def)
    {
        string keyToLookUp = string.Format ("collected_{0}", def.Type);

        bool isNew = !PlayerPrefs.HasKey (keyToLookUp);

        if (isNew) 
        {
            PlayerPrefs.SetInt (keyToLookUp, 1);
        }

        if(!collectedPowerUps.ContainsKey (def.Type))
        {
            collectedPowerUps[def.Type] = new PowerUpStat{ Type = def.Type, Amount = 1 };
        }
        else
        {
            collectedPowerUps[def.Type].Amount++;
        }

        OnPowerUpStatUpdated(def.Type,collectedPowerUps[def.Type]);
        return isNew;
    }

    public bool IsPowerUpCollected(PowerUpType type)
    {
        string keyToLookUp = string.Format ("collected_{0}", type);
        return PlayerPrefs.HasKey (keyToLookUp) || collectedPowerUps.ContainsKey (type);
    }

    public void AddEnemyDefeated(){
        CurrentGameStats.EnemiesDefeated++;
    }

    public override void OnDestroy()
    {
        PlayerPrefs.Save ();
        m_Instance = null; // this singleton can be destroyed safely
    }
}
