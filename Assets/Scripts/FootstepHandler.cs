﻿using UnityEngine;

public class FootstepHandler : MonoBehaviour {

    [SerializeField] bool useCustomSounds;
    [SerializeField] AudioClip[] customSounds;

    Transform m_cachedTransform;

    void Awake()
    {
        m_cachedTransform = this.transform;
    }

    public void OnFootstep()
    {
        if (!useCustomSounds) 
        {
            AudioClip clipToPlay = StepsManager.Instance.GetSfxForTerrainAtPosition (this.m_cachedTransform.position);
            if (clipToPlay != null) 
            {
                AudioManager.Instance.PlayClipAtLocation (clipToPlay, this.m_cachedTransform, false);
            }
        } 
        else 
        {
            AudioManager.Instance.PlayClipAtLocation (customSounds.GetRandom (), this.m_cachedTransform, false);
        }
    }
}