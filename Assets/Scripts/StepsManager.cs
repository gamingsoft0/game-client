﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Steps manager.
/// <para> based on the answer here : http://answers.unity3d.com/questions/34328/terrain-with-multiple-splat-textures-how-can-i-det.html</para>
/// <para>Also is based on this: https://forum.unity3d.com/threads/detecting-terrain-texture-at-position.94723/</para>
/// </summary>
[ExecuteInEditMode]
public class StepsManager : Singleton<StepsManager> 
{
    [System.Serializable]
    public struct TerrainSfx
    {
        public List<AudioClip> clips;
    }

    [SerializeField] Terrain terrain;
    [SerializeField] TerrainData terrainData;
    float[,,] splatmapData;
    [SerializeField] float numTextures;
    [SerializeField] List<TerrainSfx> sfxPerTerrainId;

    void Start()
    {
        if (terrainData == null) { terrainData = Terrain.activeTerrain.terrainData; }
        int alphamapWidth = terrainData.alphamapWidth;
        int alphamapHeight = terrainData.alphamapHeight;

        splatmapData = terrainData.GetAlphamaps (0, 0, alphamapWidth, alphamapHeight);
        numTextures = splatmapData.Length / (alphamapWidth * alphamapHeight);
    }

    #if UNITY_EDITOR
    void Update()
    {
        Start ();
    }
    #endif

    private Vector3 ConvertToSplatMapCoordinate(Vector3 worldPosition)
    {
        Vector3 vecRet = new Vector3();
        Terrain ter = terrain;
        Vector3 terPosition = ter.transform.position;
        vecRet.x = ((worldPosition.x - terPosition.x) / ter.terrainData.size.x) * ter.terrainData.alphamapWidth;
        vecRet.z = ((worldPosition.z - terPosition.z) / ter.terrainData.size.z) * ter.terrainData.alphamapHeight;
        return vecRet;
    }

    int GetActiveTerrainTextureIdx(Vector3 worldPosition)
    {        
        Vector3 TerrainCord = ConvertToSplatMapCoordinate(worldPosition);
        int ret = 0;
        float comp = 0f;
        for (int i = 0; i < numTextures; i++)
        {
            if (comp < splatmapData[(int)TerrainCord.z, (int)TerrainCord.x, i])
                ret = i;
        }
        return ret;
    }

    public AudioClip GetSfxForTerrainAtPosition(Vector3 worldPosition)
    {
        int indexToUse = GetActiveTerrainTextureIdx (worldPosition);
        if (indexToUse >= 0 && sfxPerTerrainId.Count > indexToUse) 
        {
            return sfxPerTerrainId [indexToUse].clips.GetRandom ();
        }
        return null;
    }

    public override void OnDestroy ()
    {
        m_Instance = null; // this way singleton can be removed safely
    }
}
