﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerActionOnTrigger : MonoBehaviour {

    [SerializeField] UnityEngine.Events.UnityEvent m_Actions;
    [SerializeField] bool m_OneShoot;
    [SerializeField] string tagToCheck;

    void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag (tagToCheck)) 
        {
            return; // invalid 
        }

        m_Actions.Invoke();
        if(m_OneShoot)
        {
            gameObject.SetActive(false);
        }
    }
}
