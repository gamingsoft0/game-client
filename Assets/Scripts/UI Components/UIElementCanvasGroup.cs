﻿using System.Collections;
using UnityEngine;
using DG.Tweening;

[RequireComponent(typeof(CanvasGroup))]
public class UIElementCanvasGroup : UIElementBase
{
    [SerializeField] float showDelay = 0.5f;
    [SerializeField] float hideDelay = 0.1f;

    [SerializeField] CanvasGroup m_viewContainer;

    public override bool IsBeingShown => m_viewContainer != null && m_viewContainer.alpha == 1;

    private void Awake()
    {
        if (m_viewContainer == null) 
        {   // get component if not already set
            m_viewContainer = GetComponent<CanvasGroup>(); 
        }
    }

    [ContextMenu("Show")]
    public override void Show()
    {
        if (showDelay > 0 && Application.isPlaying)
        {
            StartCoroutine(ShowHideAnimated(true, showDelay));
        }
        else { ApplyFinalValues(true); }
    }

    [ContextMenu("Hide")]
    public override void Hide()
    {
        if (hideDelay > 0 && Application.isPlaying)
        {
            StartCoroutine(ShowHideAnimated(false, hideDelay));
        }
        else { ApplyFinalValues(false); }
    }

    IEnumerator ShowHideAnimated(bool state, float seconds) 
    {
        if (m_viewContainer == null) 
        {   // this could happen if this call is made before component initialization
            yield return new WaitForSecondsRealtime(0.1f);
        }

        if (m_viewContainer == null) 
        {   // cancel show/hide action
            Debug.LogError("Unable to show/hide element. view is null",this);
            yield break; 
        }
        
        // set initial values
        float initialValue = state ? 0f : 1f;
        float finalValue = state ? 1f : 0f;        

        if (seconds > 0)
        {   // block ui while animating
            m_viewContainer.alpha = initialValue;
            m_viewContainer.blocksRaycasts = false;
            m_viewContainer.interactable = false;
            // trigger animation and wait for it
            var tween = m_viewContainer.DOFade(finalValue, seconds).SetUpdate(true).Pause();
            tween.startValue = initialValue;
            tween.endValue = finalValue;
            yield return null;
            tween.Play();
            yield return new WaitWhile(tween.IsPlaying);
        }

        // set final values
        ApplyFinalValues(state);
    }

    void ApplyFinalValues(bool state) 
    {
        m_viewContainer.alpha = state ? 1 : 0;
        m_viewContainer.interactable = state;
        m_viewContainer.blocksRaycasts = state;
    }
}
