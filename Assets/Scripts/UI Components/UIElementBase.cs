﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class UIElementBase : MonoBehaviour
{
    public abstract bool IsBeingShown { get; }
    public abstract void Show();
    public abstract void Hide();
}
