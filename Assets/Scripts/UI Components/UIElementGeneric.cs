﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIElementGeneric : UIElementBase
{
    [SerializeField] GameObject m_viewContainer;

    public override bool IsBeingShown => m_viewContainer != null && m_viewContainer.activeSelf;

    public override void Hide()
    {
        m_viewContainer.SetActive(false);
    }

    public override void Show()
    {
        m_viewContainer.SetActive(true);
    }
}
