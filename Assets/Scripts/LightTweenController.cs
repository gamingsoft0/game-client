﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

[RequireComponent(typeof(Light))]
public class LightTweenController : MonoBehaviour 
{
    [System.Serializable]
    public struct ColorTweenConfig
    {
        public string Name; // just to be shown at inspector
        public Color Color;
        public float Delay;
    }

    private Light m_Light;
    [SerializeField] ColorTweenConfig[] m_TweenConfigs;

    void Start()
    {
        m_Light = GetComponent<Light> ();
    }

    public void TweenColor(int tweenIndex)
    {
        this.m_Light.DOColor (m_TweenConfigs[tweenIndex].Color, m_TweenConfigs[tweenIndex].Delay);
    }
}
