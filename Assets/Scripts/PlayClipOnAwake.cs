﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayClipOnAwake : MonoBehaviour 
{
    [SerializeField] AudioClip[] clipToBePlayed;
    [SerializeField] bool followThisObject;

    void Awake()
    {
        AudioManager.Instance.PlayClipAtLocation (clipToBePlayed.GetRandom (), this.transform, followThisObject);
    }
}
