Links to external resources used in the game

- CineMachine: https://www.assetstore.unity3d.com/en/#!/content/79898
- Post Processing Stack : https://www.assetstore.unity3d.com/en/#!/content/83912
- PolyCount Reduction Tools:  https://www.mixamo.com/decimator / http://www.mesh-online.net
- DOTween: https://www.assetstore.unity3d.com/en/#!/content/27676
- Normal Map Maker: https://www.assetstore.unity3d.com/en/#!/content/10388
- Warrior Pack Bundle 2 (Animations) https://www.assetstore.unity3d.com/en/#!/content/42454
- Double Axe: https://www.assetstore.unity3d.com/en/#!/content/4039
- Fantasy Treasure Pack Lite: https://www.assetstore.unity3d.com/en/#!/content/80898
- Game Icons: https://www.assetstore.unity3d.com/en/#!/content/91138
- PowerUp Particles: https://www.assetstore.unity3d.com/en/#!/content/16458

- 3D Characters
- Fantasy Monster Skeleton: https://www.assetstore.unity3d.com/en/#!/content/35635
- Goblin Robber: https://www.assetstore.unity3d.com/en/#!/content/66959
- Mini Orc: https://www.assetstore.unity3d.com/en/#!/content/25644
- Golem: https://www.assetstore.unity3d.com/en/#!/content/33260

- Cartoon Wizard Free (rigged using Maya) : https://www.cgtrader.com/free-3d-models/character/fantasy/cartoon-wizzard
- Mask Boy (fully rigged using maya. includes fingers) : https://www.cgtrader.com/free-3d-models/character/fantasy/mask-boy


- 3D Environment
- Toon Forest Free: https://www.assetstore.unity3d.com/en/#!/content/66124
- Cartoon Nature Pack: https://www.assetstore.unity3d.com/en/#!/content/57365
- Make Your Fantasy Game - Lite: https://www.assetstore.unity3d.com/en/#!/content/8312
- stylized Trees (Low Poly): https://www.assetstore.unity3d.com/en/#!/content/49916
- Sunny Village Lite: https://www.assetstore.unity3d.com/en/#!/content/85199


- Shaders
- Toony Colors Free: https://www.assetstore.unity3d.com/en/#!/content/3926
  - Regular, Rim Lightning and Rim Outline modified to support cutout and bump mapping


- Usefull info
  - Main Config file is located in: Assets/GamePlay/GameDataDB.asset
  - You can open the custom editor by double clicking the file
  - You can also access specific editors by editing a CharacterEntity or a PowerUpDefinition, by clicking the "Edit" button on their custom inspectors

  - There is al support for footstep system, activated by triggering game events from animations.
  - Prior system (animation game events) is also used for combat system

  - Cinemachine is being used to handle player's camera tracking. This system can be used to target more than one entity at a time.
  - An example for this can be seen while fightning against the final boss of the game (the big golem at the end). When he is targeting you
  - the camera focuses two of you, creating an smooth camera zoom in/out when near/far the enemy.

  - Finally, there is a bit of day/night cycle, while playing the game. At certain points, light changes from daylight, to sunset to night and the to daylight again
 - 
