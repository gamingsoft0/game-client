﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Extensions  
{
    public static T GetRandom<T>(this IList<T> objects) 
    {
        if (objects == null || objects.Count == 0) {
            return default(T);
        }

        int indexToReturn = Random.Range(0, objects.Count);
        return objects[indexToReturn];
    }

    public static T TryGetValue<U,T>(this Dictionary<U, T> dict, U key) 
    {
        if (dict == null || dict.Count == 0 || !dict.ContainsKey(key)) 
        {
            return default(T);
        }

        return dict[key];
    }

    public static void DestroyChildren(this Transform parent)
    {
        while (parent.transform.childCount > 0)
        {
            Transform t = parent.GetChild(0);
            t.gameObject.SetActive(false);
            t.parent = null;    // reparent, which will change child count immediately.
            if (Application.isPlaying) { Object.Destroy(t.gameObject); }
            else { Object.DestroyImmediate(t.gameObject); }
        }
    }

    /// <summary>
    /// Sets a custom layer by layer name
    /// </summary>
    /// <param name="parent">Parent.</param>
    /// <param name="layerName">Layer.</param>
    /// <param name="includeChildren">If set to <c>true</c> include children objects.</param>
    public static void SetLayer(this GameObject parent, string layerName, bool includeChildren = true)
    {
        int layer = LayerMask.NameToLayer(layerName);
        SetLayer(parent, layer, includeChildren);
    }

    /// <summary>
    /// Sets a custom layer by layer id
    /// </summary>
    /// <param name="parent">Parent.</param>
    /// <param name="layer">Layer.</param>
    /// <param name="includeChildren">If set to <c>true</c> include children objects.</param>
    public static void SetLayer(this GameObject parent, int layer, bool includeChildren = true)
    {
        parent.layer = layer;
        if (includeChildren)
        {
            foreach (Transform item in parent.transform)
            {
                item.gameObject.layer = layer;
            }
        }
    }

    /// <summary>
    /// Sets a custom by tag name
    /// </summary>
    /// <param name="parent">Parent.</param>
    /// <param name="tag">Tag.</param>
    /// <param name="includeChildren">If set to <c>true</c> include children.</param>
    public static void SetTag(this GameObject parent, string tag, bool includeChildren = true)
    {
        parent.tag = tag;
        if(includeChildren)
        {
            foreach(Transform item in parent.transform)
            {
                item.gameObject.tag = tag;
            }
        }
    }

    /// <summary>
    /// https://wiki.unity3d.com/index.php/GetOrAddComponent
    /// Returns the component of Type type. If one doesn't already exist on the GameObject it will be added.
    /// </summary>
    /// <typeparam name="T">The type of Component to return.</typeparam>
    /// <param name="gameObject">The GameObject this Component is attached to.</param>
    /// <returns>Component</returns>
    static public T GetOrAddComponent<T>(this GameObject gameObject) where T : Component
    {
        return gameObject.GetComponent<T>() ?? gameObject.AddComponent<T>();
    }
}
