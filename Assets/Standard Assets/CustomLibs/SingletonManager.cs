using UnityEngine;
using System.Collections;

public class SingletonManager
{
    private static GameObject _gameObject = null;
    public static GameObject gameObject
    {
        get
        {
            if (_gameObject == null)
            {
                _gameObject = new GameObject("-SingletonManager");
                if (Application.isPlaying) {
                    Object.DontDestroyOnLoad(_gameObject);
                }
                _gameObject.hideFlags = HideFlags.DontSaveInEditor;
            }
            return _gameObject;
        }
    }
}
