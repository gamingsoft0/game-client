using System;
using UnityEngine;
using System.Collections;

/// <summary>
/// Prefab attribute. Use this on child classes
/// to define if they have a prefab associated or not
/// By default will attempt to load a prefab
/// that has the same name as the class,
/// otherwise [Prefab("path/to/prefab")] to define it specifically. 
/// </summary>
[AttributeUsage(AttributeTargets.Class, Inherited = true)]
public class PrefabAttribute : Attribute
{
    string _name;

    public string Name { get { return this._name; } }

    public PrefabAttribute()
    {
        this._name = "";
    }

    public PrefabAttribute(string name)
    {
        this._name = name;
    }
}

public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
    protected static T m_Instance;
    private static object _lock = new object();

    public static bool IsAwake { get { return (m_Instance != null); } }

    protected virtual void Awake()
    {
        if(m_Instance != null)
        {
            // Use new instance instead of the old one
            if(Application.isPlaying)
            {
                UnityEngine.Object.Destroy(m_Instance.gameObject);
            }
            else
            {
                UnityEngine.Object.DestroyImmediate(m_Instance.gameObject);
            }
        }

        m_Instance = this as T;
    }

    public static T Instance
    {
        get
        {
            if(applicationIsQuitting)
            {
                Debug.LogWarningFormat("[Singleton] Instance [{0}] already destroyed on application quit. Won't create again - returning null.", typeof(T));
                return null;
            }

            lock(_lock)
            {
                if(!m_Instance)
                {
                    GameObject newGo = null;
                    Type mytype = typeof(T);
                    string goName = mytype.ToString();

                    bool hasPrefab = Attribute.IsDefined(mytype, typeof(PrefabAttribute));
                    // checks if the [Prefab] attribute is set and pulls that if it can
                    if(hasPrefab)
                    {
                        PrefabAttribute attr = (PrefabAttribute)Attribute.GetCustomAttribute(mytype, typeof(PrefabAttribute));
                        string prefabname = attr.Name;                  
                        try
                        {
                            if(!string.IsNullOrEmpty(prefabname))
                            {
                                newGo = (GameObject)Instantiate(Resources.Load(prefabname, typeof(GameObject)));
                            }
                            else
                            {
                                newGo = (GameObject)Instantiate(Resources.Load(goName, typeof(GameObject)));
                            }
                            newGo.transform.SetParent(SingletonManager.gameObject.transform);
                            m_Instance = newGo.GetComponentInChildren<T>();
                        }
                        catch(Exception e)
                        {
                            Debug.LogError("could not instantiate prefab even though prefab attribute was set: " + e.Message + "\n" + e.StackTrace);
                        }
                    }
                    else
                    {
                        newGo = new GameObject(goName, typeof(T));
                        m_Instance = newGo.AddComponent<T>();
                        newGo.transform.SetParent(SingletonManager.gameObject.transform);
                    }
                }
            }                
            return m_Instance;
        }
    }

    private static bool applicationIsQuitting = false;

    /// <summary>
    /// When Unity quits, it destroys objects in a random order.
    /// In principle, a Singleton is only destroyed when application quits.
    /// If any script calls Instance after it have been destroyed, 
    ///   it will create a buggy ghost object that will stay on the Editor scene
    ///   even after stopping playing the Application. Really bad!
    /// So, this was made to be sure we're not creating that buggy ghost object.
    /// </summary>
    public virtual void OnDestroy()
    {
        applicationIsQuitting = true;
    }
}